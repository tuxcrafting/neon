module kernel.fd;

import kernel.config;
import kernel.fcntl;
import kernel.stat;
import kernel.types;

@nogc:
nothrow:

enum FDType {
    Fifo,
    ChrDev,
    Dir,
    BlkDev,
    Reg,
    Lnk,
    Sock,
}

struct FileDescriptor {
    long refcnt;
    FDType type;
    int options;

    off_t offset;
    void* data;

    size_t pathLength;
    ubyte[MAX_PATH_LENGTH] path;

    @nogc:
    nothrow:

    ssize_t function(FileDescriptor*, void*, size_t) write;
    ssize_t function(FileDescriptor*, void*, size_t) read;
    off_t function(FileDescriptor*, off_t, int) seek;
    int function(FileDescriptor*, stat_t*) stat;
    int function(FileDescriptor* fd) close;

    int safeClose() {
        refcnt--;
        if (refcnt == 0) {
            if (close != null) {
                return (*close)(&this);
            }
        }
        return 0;
    }
}

__gshared FileDescriptor[MAX_FD] gfds;

FileDescriptor* allocGfd() {
    for (int i = 0; i < MAX_FD; i++) {
        if (gfds[i].refcnt == 0) {
            return &gfds[i];
        }
    }
    return null;
}

struct DirectoryEntry {
    ino_t ino;
    char[MAX_PATH_LENGTH] path;
}
