module kernel.random;

import kernel.types;

@nogc:
nothrow:

__gshared ulong[2] xs_seed;

void randSeed(ulong seed1, ulong seed2) {
    xs_seed[0] = seed1;
    xs_seed[1] = seed2;
}

ulong rand() {
    ulong x = xs_seed[0];
    ulong y = xs_seed[1];
    xs_seed[0] = y;
    x ^= x << 32;
    xs_seed[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
    return xs_seed[1] + y;
}
