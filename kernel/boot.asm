section .text
bits 32

global _start, p_pml4, asm_loadTSS, stack_end
extern kmain, _data_end, _bss_end, _end

MULTIBOOT_HEADER_MAGIC equ 0x1BADB002
MULTIBOOT_HEADER_FLAGS equ 0x00000003

KERNEL_VMA_BASE equ 0xFFFF800000000000
KERNEL_LMA_BASE equ 0x100000

align 4
multiboot_header:
    dd MULTIBOOT_HEADER_MAGIC
    dd MULTIBOOT_HEADER_FLAGS
    dd -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

_start:
    cli
    ; save multiboot structure address
    mov esi, ebx
    mov edi, eax
    ; enable PAE
    mov eax, cr4
    bts eax, 5
    mov cr4, eax
    ; set page table address
    mov eax, p_pml4 - KERNEL_VMA_BASE
    mov cr3, eax
    ; enable long mode
    mov ecx, 0xC0000080
    rdmsr
    bts eax, 8
    wrmsr
    ; load gdt
    lgdt [gdt_desc - KERNEL_VMA_BASE]
    ; enable paging
    mov eax, cr0
    bts eax, 31
    mov cr0, eax

    jmp gdt.CS_KERNEL:start64 - KERNEL_VMA_BASE

bits 64

start64:
    ; set segments to the new DS
    mov ax, gdt.DS_KERNEL
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    jmp qword [start64_next_addr - KERNEL_VMA_BASE]
.next:
    ; unmap low identity mapping
    mov rax, p_pml4
    mov qword [rax], 0
    mov rax, p_pml4 - KERNEL_VMA_BASE
    mov cr3, rax
    ; set new stack
    mov rsp, stack_end
    ; reload gdt to the upper part of memory
    mov rax, gdt_desc
    mov rdx, gdt
    mov qword [rax+2], rdx
    lgdt [rax]
    jmp kmain

asm_loadTSS:
    mov ax, gdt.TSS
    ltr ax
    ret

start64_next_addr dq start64.next

section .data

gdt_desc:
    dw gdt.end - gdt - 1
    dq gdt - KERNEL_VMA_BASE
align 8
gdt:
.NULL equ $ - gdt
    dq 0x0000000000000000, 0
.CS_KERNEL equ $ - gdt
    dq 0x00AF9A000000FFFF, 0
.DS_KERNEL equ $ - gdt
    dq 0x00AF93000000FFFF, 0
.CS_USER equ $ - gdt
    dq 0x00AFFA000000FFFF, 0
.DS_USER equ $ - gdt
    dq 0x00AFF3000000FFFF, 0
.TSS equ $ - gdt
    dq 0x0040890000000067, 0
.end:

align 4096
p_pml4:
    dq p_pdp - KERNEL_VMA_BASE + 0x3
    times 255 dq 0
    dq p_pdp - KERNEL_VMA_BASE + 0x3
    times 255 dq 0

align 4096
p_pdp:
    dq p_pd - KERNEL_VMA_BASE + 0x3
    times 511 dq 0

align 4096
p_pd:
    dq p_p - KERNEL_VMA_BASE + 0x3
    %assign i 4096 * 512
    %rep 511
    dq i + 0x83
    %assign i i + 4096 * 512
    %endrep

align 4096
p_p:
    dq 0 ; don't page to catch null pointers with a #PF
    %assign i 4096
    %rep 511
    dq i + 0x3
    %assign i i + 4096
    %endrep

section .bss

stack:
    resb 65536*8
stack_end:
