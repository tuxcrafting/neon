module kernel.wait;

enum {
    P_PID,
    P_PGID,
    P_ALL,
}

enum {
    WCONTINUED = 0x01,
    WEXITED = 0x02,
    WNOHANG = 0x04,
    WNOWAIT = 0x08,
    WSTOPPED = 0x10,
}
