module kernel.scheduler;

import kernel.config;
import kernel.process;
import kernel.queue;
import kernel.types;
import kernel.util;

@nogc:
nothrow:

__gshared Queue!(pid_t, MAX_PID) queueMain, queueHold;

__gshared bool scheduling = false;

// flat round-robin scheduler
// until the main queue is empty, dequeue a process from the main queue
// execute it and enqueue it in the hold queue
// when the main queue is empty, move the hold queue to the main queue
// and then repeat
void schedule() {
    for (;;) {
        if (queueMain.empty) {
            if (queueHold.empty) {
                if (!scheduling) {
                    return;
                }
                panic(KS!"Scheduler queue is empty");
            }
            while (!queueHold.empty) {
                queueMain.enqueue(queueHold.dequeue);
            }
        }
        pid_t pid = queueMain.dequeue;
        Process* process = &processes[pid];
        if (process.state == ProcessState.Waiting) {
            queueHold.enqueue(pid);
            continue;
        }
        if (process.state != ProcessState.Running) {
            continue;
        }
        queueHold.enqueue(pid);
        currentPid = pid;
        process.switchTo;
    }
}

void switchOrSchedule() {
    processes[currentPid].switchTo;
    schedule;
}
