module kernel.elf;

import kernel.memory;
import kernel.mman;
import kernel.process;
import kernel.types;
import kernel.util;

@nogc:
nothrow:

extern(C) extern __gshared ulong[512] p_pml4;

enum ELFEIClass : ubyte {
    None = 0,
    ELF32,
    ELF64,
}

enum ELFEIData : ubyte {
    None = 0,
    Little,
    Big,
}

enum ELFEIVersion : ubyte {
    None = 0,
    Original,
}

enum ELFEIABI : ubyte {
    SysV = 0,
    HPUX,
    NetBSD,
    Linux,
    GNUHurd,
    Solaris,
    AIX,
    IRIX,
    FreeBSD,
    Tru64,
}

enum ELFType : ushort {
    None = 0,
    Rel,
    Exec,
    Dyn,
    Core,
}

enum ELFMachine : ushort {
    None = 0x00,
    SPARC = 0x02,
    X86 = 0x03,
    MIPS = 0x08,
    PowerPC = 0x14,
    S390 = 0x16,
    ARM = 0x28,
    SuperH = 0x2A,
    IA64 = 0x32,
    X86_64 = 0x3E,
    AArch64 = 0xB7,
    RISCV = 0xF3,
}

struct ElfHeader64 {
align(1):
    uint eiMagic;
    ELFEIClass eiClass;
    ELFEIData eiData;
    ELFEIVersion eiVersion;
    ELFEIABI eiABI;
    ubyte eiABIVersion;
    ubyte[7] eiPad;
    ELFType eType;
    ELFMachine eMachine;
    uint eVersion;
    ulong eEntry;
    ulong ePhoff;
    ulong eShoff;
    uint eFlags;
    ushort eEhsize;
    ushort ePhentsize;
    ushort ePhnum;
    ushort eShentsize;
    ushort eShnum;
    ushort eShstrndx;
}

enum ElfProgramSegmentType : uint {
    Null = 0,
    Load,
    Dynamic,
    Interp,
    Note,
    Shlib,
    Phdr,
}

enum ElfProgramHeaderFlags {
    X = 1,
    W = 2,
    R = 4,
}

struct ElfProgramHeader64 {
align(1):
    ElfProgramSegmentType pType;
    uint pFlags;
    ulong pOffset;
    ulong pVaddr;
    ulong pPaddr;
    ulong pFilesz;
    ulong pMemsz;
    ulong pAlign;
}

enum ElfSectionHeaderType : uint {
    Null = 0,
    Progbits,
    Symtab,
    Strtab,
    Rela,
    Hash,
    Dynamic,
    Note,
    Nobits,
    Rel,
    Shlib,
    Dynsym,
    ShtInitArray = 0xE,
    ShtFiniArray,
    ShtPreinitArray,
    ShtGroup,
    ShtSymtabShndx,
    ShtNum,
}

enum ElfSectionHeaderFlags {
    Write = 1,
    Alloc = 2,
    ExecInstr = 4,
    Merge = 16,
    Strings = 32,
    InfoLink = 64,
    LinkOrder = 128,
    OSNoncomforming = 256,
    Group = 512,
    TLS = 1024,
}

struct ElfSectionHeader64 {
align(1):
    uint shName;
    ulong shFlags;
    ulong shAddr;
    ulong shOffset;
    ulong shSize;
    uint shLink;
    uint shInfo;
    ulong shAddralign;
    ulong shEntsize;
}

long loadElf64(Process* process, ubyte* data) {
    ElfHeader64* header = cast(ElfHeader64*)data;
    for (size_t i = 0; i < header.ePhnum; i++) {
        ElfProgramHeader64* ph =
            cast(ElfProgramHeader64*)(data
                                    + header.ePhoff
                                    + i * ElfProgramHeader64.sizeof);
        if (ph.pType == ElfProgramSegmentType.Load) {
            if (ph.pMemsz == 0) {
                continue;
            }
            void *addr = sysMmap(
                process,
                true,
                cast(void*)(ph.pVaddr & ~4095),
                ph.pMemsz,
                ((ph.pFlags & ElfProgramHeaderFlags.X) ? PROT_EXEC : 0) |
                ((ph.pFlags & ElfProgramHeaderFlags.R) ? PROT_READ : 0) |
                ((ph.pFlags & ElfProgramHeaderFlags.W) ? PROT_WRITE : 0),
                MAP_FIXED,
                MAPFD_ANON,
                0);
            if (cast(ssize_t)addr < 0) {
                return 1;
            }
            memcpy(
                getVMA(addr + (ph.pVaddr & 4095)),
                data + ph.pOffset,
                ph.pFilesz);
        }
    }
    void* stack = sysMmap(
        process,
        false,
        null,
        16 * 1024,
        PROT_READ | PROT_WRITE,
        0,
        MAPFD_ANON,
        0);
    if (cast(ssize_t)stack < 0) {
        return 1;
    }
    process.rip = header.eEntry;
    process.rsp = cast(size_t)stack + 16 * 1024;
    return 0;
}
