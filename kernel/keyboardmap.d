module kernel.keyboardmap;

@nogc:
nothrow:

char parseScancode(ubyte scancode, bool shift, bool ext) {
    if (ext) {
        switch (scancode) {
        case 0x4A:
            return '/';
        case 0x5A:
            return '\r';
        default:
            return 0;
        }
    }
    switch (scancode) {
    case 0x1C:
        return shift ? 'A' : 'a';
    case 0x32:
        return shift ? 'B' : 'b';
    case 0x21:
        return shift ? 'C' : 'c';
    case 0x23:
        return shift ? 'D' : 'd';
    case 0x24:
        return shift ? 'E' : 'e';
    case 0x2B:
        return shift ? 'F' : 'f';
    case 0x34:
        return shift ? 'G' : 'g';
    case 0x33:
        return shift ? 'H' : 'h';
    case 0x43:
        return shift ? 'I' : 'i';
    case 0x3B:
        return shift ? 'J' : 'j';
    case 0x42:
        return shift ? 'K' : 'k';
    case 0x4B:
        return shift ? 'L' : 'l';
    case 0x3A:
        return shift ? 'M' : 'm';
    case 0x31:
        return shift ? 'N' : 'n';
    case 0x44:
        return shift ? 'O' : 'o';
    case 0x4D:
        return shift ? 'P' : 'p';
    case 0x15:
        return shift ? 'Q' : 'q';
    case 0x2D:
        return shift ? 'R' : 'r';
    case 0x1B:
        return shift ? 'S' : 's';
    case 0x2C:
        return shift ? 'T' : 't';
    case 0x3C:
        return shift ? 'U' : 'u';
    case 0x2A:
        return shift ? 'V' : 'v';
    case 0x1D:
        return shift ? 'W' : 'w';
    case 0x22:
        return shift ? 'X' : 'x';
    case 0x35:
        return shift ? 'Y' : 'y';
    case 0x1A:
        return shift ? 'Z' : 'z';
    case 0x45:
        return shift ? ')' : '0';
    case 0x16:
        return shift ? '!' : '1';
    case 0x1E:
        return shift ? '@' : '2';
    case 0x26:
        return shift ? '#' : '3';
    case 0x25:
        return shift ? '$' : '4';
    case 0x2E:
        return shift ? '%' : '5';
    case 0x36:
        return shift ? '^' : '6';
    case 0x3D:
        return shift ? '&' : '7';
    case 0x3E:
        return shift ? '*' : '8';
    case 0x46:
        return shift ? '(' : '9';
    case 0x0E:
        return shift ? '~' : '`';
    case 0x4E:
        return shift ? '_' : '-';
    case 0x55:
        return shift ? '+' : '=';
    case 0x5D:
        return shift ? '|' : '\\';
    case 0x66:
        return '\x08';
    case 0x29:
        return ' ';
    case 0x0D:
        return '\t';
    case 0x5A:
        return '\n';
    case 0x76:
        return '\x1b';
    case 0x54:
        return shift ? '{' : '[';
    case 0x7C:
        return '*';
    case 0x7B:
        return '-';
    case 0x79:
        return '+';
    case 0x71:
        return '.';
    case 0x70:
        return '0';
    case 0x69:
        return '1';
    case 0x72:
        return '2';
    case 0x7A:
        return '3';
    case 0x6B:
        return '4';
    case 0x73:
        return '5';
    case 0x74:
        return '6';
    case 0x6C:
        return '7';
    case 0x75:
        return '8';
    case 0x7D:
        return '9';
    case 0x5B:
        return shift ? '}' : ']';
    case 0x4C:
        return shift ? ':' : ';';
    case 0x52:
        return shift ? '"' : '\'';
    case 0x41:
        return shift ? '<' : ',';
    case 0x49:
        return shift ? '>' : '.';
    case 0x4A:
        return '/';
    default:
        return 0;
    }
}
