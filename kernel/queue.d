module kernel.queue;

struct Queue(T, size_t N) {
    T[N] array;
    size_t a, b;

    void enqueue(T x) {
        array[a++] = x;
        a %= N;
    }

    T dequeue() {
        T x = array[b++];
        b %= N;
        return x;
    }

    bool empty() {
        return a == b;
    }
}
