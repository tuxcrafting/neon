module kernel.main;

import kernel.config;
import kernel.cpio;
import kernel.elf;
import kernel.errno;
import kernel.fcntl;
import kernel.fd;
import kernel.fs;
import kernel.fs.cpio;
import kernel.fs.dev;
import kernel.interrupt;
import kernel.io;
import kernel.keyboardmap;
import kernel.memory;
import kernel.multiboot;
import kernel.process;
import kernel.random;
import kernel.scheduler;
import kernel.signal;
import kernel.stat;
import kernel.syscall;
import kernel.syscall.all;
import kernel.types;
import kernel.util;
import kernel.vga;

@nogc:
nothrow:

extern(C) extern __gshared ulong[512] p_pml4;
extern(C) extern __gshared ubyte _end;

enum size_t PIT_BASE_FREQ = 1193182;
enum ushort PIT_DIVIDER = PIT_BASE_FREQ / PIT_FREQUENCY;

__gshared bool kbshift, kbext, kbignore;
__gshared char[128] keyboardBuffer;
__gshared size_t keyboardBufferI;
__gshared Process* kbprocess;

void picHandler() {
    irqEOI(0);
    cprocess = &processes[currentPid];
    cprocess.saveFrom(&picHandler_r);
    schedule;
}

void kbHandler() {
    ubyte p = portIn(0x60);
    irqEOI(0);
    if (kbignore) {
        kbignore = false;
        if (p == 0x12 || p == 0x59) {
            kbshift = false;
        }
        return;
    }
    if (p == 0x12 || p == 0x59) {
        kbshift = true;
        return;
    }
    if (p == 0xF0) {
        kbignore = true;
        return;
    }
    if (p == 0xE0) {
        kbext = true;
        return;
    }
    char c = parseScancode(p, kbshift, kbext);
    if (kbext) {
        kbext = false;
    }
    if (c == '\x08') {
        if (keyboardBufferI > 0) {
            keyboardBuffer[keyboardBufferI] = 0;
            keyboardBufferI--;
            write(c);
        }
        return;
    }
    if (keyboardBufferI < 128) {
        keyboardBuffer[keyboardBufferI++] = c;
        keyboardBuffer[keyboardBufferI + 1] = 0;
        write(c);
        if (c == '\n') {
            if (kbprocess != null) {
                kbprocess.state = ProcessState.Running;
                kbprocess = null;
            }
        }
    }
}

void deHandler() {
    if (deHandler_r.cs == SEG_CS_KERNEL) {
        panic(KS!"Kernel divide by zero");
    }
    cprocess = &processes[currentPid];
    cprocess.saveFrom(&deHandler_r);
    cprocess.clearSignalInfo;
    cprocess.siginfo.si_code = FPE_INTDIV;
    cprocess.siginfo.si_addr = cast(void*)deHandler_r.rip;
    if (cprocess.kill(SIGFPE) == 1) {
        switchOrSchedule;
    }
};
void udHandler() {
    if (udHandler_r.cs == SEG_CS_KERNEL) {
        panic(KS!"Kernel illegal opcode");
    }
    cprocess = &processes[currentPid];
    cprocess.saveFrom(&udHandler_r);
    cprocess.clearSignalInfo;
    cprocess.siginfo.si_addr = cast(void*)udHandler_r.rip;
    if (cprocess.kill(SIGILL) == 1) {
        switchOrSchedule;
    }
};
void pfHandler() {
    if (pfHandler_r.cs == SEG_CS_KERNEL) {
        panic(KS!"Kernel page fault");
    }
    cprocess = &processes[currentPid];
    ulong cr2;
    asm @nogc nothrow {
        mov RAX, CR2;
        mov cr2, RAX;
    }
    cprocess.saveFrom(&pfHandler_r);
    cprocess.clearSignalInfo;
    cprocess.siginfo.si_addr = cast(void*)cr2;
    if (cprocess.kill(SIGSEGV) == 1) {
        switchOrSchedule;
    }
};
void gpfHandler() {
    if (gpfHandler_r.cs == SEG_CS_KERNEL) {
        panic(KS!"Kernel protection fault");
    }
    cprocess = &processes[currentPid];
    cprocess.saveFrom(&gpfHandler_r);
    cprocess.clearSignalInfo;
    cprocess.siginfo.si_addr = cast(void*)gpfHandler_r.rip;
    if (cprocess.kill(SIGILL) == 1) {
        switchOrSchedule;
    }
};

mixin(wrapInterrupt!"deHandler");
mixin(wrapInterrupt!"udHandler");
mixin(wrapInterrupt!("gpfHandler", true, true));
mixin(wrapInterrupt!("pfHandler", true, true));
mixin(wrapInterrupt!"picHandler");
mixin(wrapInterrupt!"kbHandler");

size_t syscallSelect(
        size_t sysno,
        size_t a, size_t b, size_t c, size_t d,
        size_t e, size_t f, size_t g) {
    cprocess = &processes[currentPid];
    switch (sysno) {
    case SYSCALL_EXIT:
        syscallExit(
            cast(int)a);
        return cast(size_t)-1;
    case SYSCALL_GETPID:
        return cast(size_t)syscallGetpid;
    case SYSCALL_FORK:
        cprocess.saveFrom(&syscallHandler_r);
        return cast(size_t)syscallFork;
    case SYSCALL_WAITID:
        cprocess.saveFrom(&syscallHandler_r);
        return cast(size_t)syscallWaitid(
            cast(idtype_t)a,
            cast(id_t)b,
            cast(siginfo_t*)c,
            cast(int)d);
    case SYSCALL_FEXECVE:
        return cast(int)syscallFexecve(
            cast(int)a,
            cast(char**)b,
            cast(char**)c);

    case SYSCALL_OPENAT:
        return cast(size_t)syscallOpenat(
            cast(int)a,
            cast(char*)b,
            cast(size_t)c,
            cast(int)d,
            cast(mode_t)e);
    case SYSCALL_CLOSE:
        return cast(size_t)syscallClose(
            cast(int)a);
    case SYSCALL_WRITE:
        return cast(size_t)syscallWrite(
            cast(int)a,
            cast(void*)b,
            cast(size_t)c);
    case SYSCALL_READ:
        return cast(size_t)syscallRead(
            cast(int)a,
            cast(void*)b,
            cast(size_t)c);
    case SYSCALL_LSEEK:
        return cast(size_t)syscallLseek(
            cast(int)a,
            cast(off_t)b,
            cast(int)c);
    case SYSCALL_FSTAT:
        return cast(size_t)syscallFstat(
            cast(int)a,
            cast(stat_t*)b);

    case SYSCALL_SIGACTION:
        return cast(size_t)syscallSigaction(
            cast(int)a,
            cast(sigaction_t*)b,
            cast(sigaction_t*)c);
    case SYSCALL_KILL:
        return cast(size_t)syscallKill(
            cast(pid_t)a,
            cast(int)b);

    case SYSCALL_MMAP:
        return cast(size_t)syscallMmap(
            cast(void*)a,
            cast(size_t)b,
            cast(int)c,
            cast(int)d,
            cast(int)e,
            cast(off_t)f);
    case SYSCALL_MUNMAP:
        return cast(size_t)syscallMunmap(
            cast(void*)a,
            cast(size_t)b);

    case SYSCALL_SIGRETURN:
        syscallSigreturn;
        return 0;

    default:
        return cast(size_t)-ENOSYS;
    }
}

void syscallHandler() {
    syscallHandler_r.rax = syscallSelect(
        syscallHandler_r.rax,
        syscallHandler_r.rdi,
        syscallHandler_r.rsi,
        syscallHandler_r.rdx,
        syscallHandler_r.r8,
        syscallHandler_r.r9,
        syscallHandler_r.r10,
        syscallHandler_r.r11);
}

mixin(wrapInterrupt!"syscallHandler");

__gshared MultibootInfo* mbi;

extern(C) void kmain(uint multibootMagic, uint multibootBase, size_t gdt) {
    mbi = cast(MultibootInfo*)(multibootBase + KERNEL_VMA_BASE);

    // init vga
    clear;
    cursorEnable(1, 14);
    write(KS!"/\\/\\ Neon /\\/\\\n");

    // disable PS/2 controller
    portOut(0x64, 0xA7);
    portOut(0x64, 0xAD);
    portIn(0x60);
    // disable scancode translation
    portOut(0x64, 0x20);
    while ((portIn(0x64) & 1) == 0) {}
    ubyte conf = portIn(0x60);
    conf &= ~(1 << 6);
    portOut(0x64, 0x60);
    while ((portIn(0x64) & 1) != 0) {}
    portOut(0x60, conf);
    // enable first PS/2 port
    portOut(0x64, 0xAE);

    // setup interrupts
    write(KS!"Setting up interrupts...\n");
    irqRemap(0x20);
    // PIT, PS/2 keyboard
    irqMask(0xFFFF & ~(1<<0) & ~(1<<1));
    memset(&idt, 0, idt.sizeof);
    // #DE
    interruptSetDescriptor(
        0x00, cast(size_t)&deHandler_h, SEG_CS_KERNEL, 0,
        true, 0, false, GateType.TrapGate32);
    // #UD
    interruptSetDescriptor(
        0x06, cast(size_t)&udHandler_h, SEG_CS_KERNEL, 0,
        true, 0, false, GateType.TrapGate32);
    // #GPF
    interruptSetDescriptor(
        0x0D, cast(size_t)&gpfHandler_h, SEG_CS_KERNEL, 0,
        true, 0, false, GateType.TrapGate32);
    // #PF
    interruptSetDescriptor(
        0x0E, cast(size_t)&pfHandler_h, SEG_CS_KERNEL, 0,
        true, 0, false, GateType.TrapGate32);
    // PIT
    interruptSetDescriptor(
        0x20, cast(size_t)&picHandler_h, SEG_CS_KERNEL, 0,
        true, 0, false, GateType.IntGate32);
    // PS/2 keyboard
    interruptSetDescriptor(
        0x21, cast(size_t)&kbHandler_h, SEG_CS_KERNEL, 0,
        true, 0, false, GateType.IntGate32);
    // syscall
    interruptSetDescriptor(
        0x80, cast(size_t)&syscallHandler_h, SEG_CS_KERNEL, 0,
        true, 3, false, GateType.IntGate32);
    interruptInit;
    tssInit(gdt);

    pitSetDivider(PIT_DIVIDER);

    // scan modules for initrd, and then copy it if found
    write(KS!"Trying to find an initrd...\n");
    MultibootModList* modules =
        cast(MultibootModList*)(mbi.modsAddr + KERNEL_VMA_BASE);
    ubyte* initrd = null;
    size_t initrdLength;
    for (size_t i = 0; i < mbi.modsCount; i++) {
        ubyte* data = cast(ubyte*)(modules[i].modStart + KERNEL_VMA_BASE);
        if (parseOctal(cast(char*)data, 6) == 29127) {
            initrd = data;
            initrdLength = modules[i].modEnd - modules[i].modStart;
            break;
        }
    }
    if (initrd == null) {
        panic(KS!"Can't find an initrd");
    }
    ubyte* kernelDynDataBase = initrd + initrdLength;
    if (cast(size_t)kernelDynDataBase % BLOCK_SIZE != 0) {
        kernelDynDataBase += BLOCK_SIZE
                           - (cast(size_t)kernelDynDataBase % BLOCK_SIZE);
    }

    // detect memory
    write(KS!"Detecting memory...\n");
    MultibootMmapEntry* mmap =
        cast(MultibootMmapEntry*)(mbi.mmapAddr + KERNEL_VMA_BASE);
    long memRangeI = 0;
    size_t bitmapIndex = 0;
    while (explicitCast!(MultibootMmapEntry*, ulong)(mmap) - KERNEL_VMA_BASE
         < mbi.mmapAddr + mbi.mmapLength) {
        char[16] buf;
        itoa(mmap.addr, buf.ptr, 16, 16);
        writeRaw(buf.ptr, 16);
        write('-');
        itoa(mmap.addr + mmap.length - 1, buf.ptr, 16, 16);
        writeRaw(buf.ptr, 16);
        write(' ');
        switch (mmap.type) {
        case MultibootMemoryType.Available:
            write(KS!"Available");
            size_t base = mmap.addr & ~(BLOCK_SIZE - 1);
            size_t limit = (mmap.addr + mmap.length) & ~(BLOCK_SIZE - 1);
            if (limit > base) {
                memoryRanges[memRangeI].base = base;
                memoryRanges[memRangeI].limit = limit;
                memoryRanges[memRangeI].indexBase = bitmapIndex;
                bitmapIndex += (limit - base) / BLOCK_SIZE;
                memoryRanges[memRangeI].indexLimit = bitmapIndex;
                memRangeI++;
                totalAvailable += limit - base;
            }
            break;
        case MultibootMemoryType.Reserved:
            write(KS!"Reserved");
            break;
        case MultibootMemoryType.ACPIReclaimable:
            write(KS!"ACPI");
            break;
        case MultibootMemoryType.NVS:
            write(KS!"ROM");
            break;
        case MultibootMemoryType.BadRAM:
            write(KS!"Bad RAM");
            break;
        default:
            write(KS!"Unknown");
        }
        write('\n');
        mmap = cast(MultibootMmapEntry*)(
            explicitCast!(MultibootMmapEntry*, ulong)(mmap)
          + mmap.size
          + mmap.size.sizeof);
    }
    // initialize allocation bitmap
    allocBitmap = kernelDynDataBase;
    size_t len = totalAvailable / BLOCK_SIZE / 4;
    memset(allocBitmap, 0, len);
    kernelDynDataBase += len;
    memoryRanges[memRangeI].limit = 0;
    size_t kFinalIndex = bitmapTranslate(kernelDynDataBase - KERNEL_VMA_BASE);
    for (size_t k = 0; k < kFinalIndex; k++) {
        allocBitmap[k / 4] |= 1 << (k % 4 * 2);
        if (k == len - 1) {
            allocBitmap[k / 4] &= ~(1 << (k % 4 * 2 + 1));
        } else {
            allocBitmap[k / 4] |= 1 << (k % 4 * 2 + 1);
        }
    }
    // print memory information
    lastAllocated = kFinalIndex;
    write(KS!"Total available memory: ");
    writeSize(totalAvailable);
    write('\n');
    write(KS!"Free memory: ");
    writeSize(totalAvailable - kFinalIndex * BLOCK_SIZE);
    write('\n');

    // clear processes
    for (pid_t j = 0; j < MAX_PID; j++) {
        processes[j].pid = j;
        pgroups[j].pnum = 0;
        sessions[j].pgnum = 0;
    }

    // init random
    randSeed(0x775056EB7D090E92, 0x4167193D5C3E811B);
    for (size_t i = 0; i < 32; i++) {
        rand;
    }

    // mount initrd on /
    mountPoints[0].present = true;
    mountPoints[0].pathBaseLength = 1;
    mountPoints[0].pathBase.ptr[0] = '/';
    CpioFs* fs = cast(CpioFs*)calloc(CpioFs.sizeof);
    fs.archive = initrd;
    mountPoints[0].mpdata = fs;
    mountPoints[0].open = &cpioOpen;

    // mount devfs on /dev
    mountPoints[1].present = true;
    mountPoints[1].pathBaseLength = 4;
    memcpy(mountPoints[1].pathBase.ptr, cast(void*)"/dev".ptr, 4);
    mountPoints[1].open = &devOpen;

    // /dev/000 is vga
    devices[0].present = true;
    devices[0].open = &vgaDevOpen;
    devices[0].write = &vgaDevWrite;
    devices[0].stat = &vgaDevStat;

    // /dev/001 is keyboard
    devices[1].present = true;
    devices[1].open = &kbDevOpen;
    devices[1].read = &kbDevRead;
    devices[1].stat = &kbDevStat;

    // init init
    write(KS!"Loading init\n");
    processes[1].init;
    processes[1].rflags |= 0x200;
    processes[1].state = ProcessState.Running;
    processes[1].cwdLength = 1;
    processes[1].cwd[0] = '/';

    // set process group and session related attributes
    processes[1].pgid = 1;
    pgroups[1].leader = 1;
    pgroups[1].session = 1; 
    pgroups[1].pnum = 1;
    sessions[1].pgnum = 1;

    // put init in scheduler
    queueMain.enqueue(1);
    currentPid = 1;
    nextPid = 2;
    scheduling = true;

    write('\n');

    currentPid = 1;
    cprocess = &processes[1];

    // open and execve
    kstring initPath = KS!"/bin/init\0";
    initPath.length--;
    bypassFaultCheck = true;
    int r = syscallOpenat(
        AT_FDCWD,
        initPath.ptr,
        initPath.length,
        O_RDONLY | O_CLOEXEC,
        0);
    if (r < 0) {
        panic(KS!"Couldn't open /bin/init");
    }
    char*[2] argv = [initPath.ptr, null];
    char*[1] envp = [null];
    bypassFaultCheck = true;
    r = syscallFexecve(r, argv.ptr, envp.ptr);
    panic(KS!"Couldn't exec init");
}

void writeSize(size_t n) {
    char[64] buf;
    long i = itoa(n, buf.ptr, 64, 10);
    writeRaw(buf.ptr + i, 64 - i);
    write(KS!" bytes");
    long suffixI;
    for (suffixI = 0;
         suffixI < SUFFIXES.length && n >= 1024;
         suffixI++) {
        n /= 1024;
    }
    if (suffixI) {
        write(KS!" (");
        i = itoa(n, buf.ptr, 64, 10);
        writeRaw(buf.ptr + i, 64 - i);
        write(' ');
        write(SUFFIXES[suffixI - 1]);
        write(KS!"iB)");
    }
}

__gshared kstring SUFFIXES = KS!"KMGT";
__gshared kstring ALPHABET = KS!"0123456789ABCDEF";

long itoa(ulong num, char* s, long length, long base) {
    memset(s, ALPHABET[0], length);
    long i;
    for (i = length - 1; i >= 0 && num; i--) {
        s[i] = ALPHABET[num % base];
        num /= base;
    }
    return i + 1;
}

T min(T)(T a, T b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

int kbDevOpen(FileDescriptor* fd, int options, mode_t mode) {
    if (options & O_WRONLY) {
        return -EINVAL;
    }
    return 0;
}
ssize_t kbDevRead(FileDescriptor* fd, void* buf, size_t length) {
    size_t mlen = min(length, keyboardBufferI);
    if (mlen != 0) {
        memcpy(buf, keyboardBuffer.ptr, mlen);
        keyboardBufferI = 0;
        memset(keyboardBuffer.ptr, 0, keyboardBuffer.length);
    } else {
        if (kbprocess != null) {
            return -EBUSY;
        }
        cprocess.saveFrom(&syscallHandler_r);
        cprocess.state = ProcessState.Waiting;
        cprocess.waitReason = WaitReason.Other;
        cprocess.rip -= 2;
        kbprocess = cprocess;
        schedule;
    }
    return mlen;
}
int kbDevStat(FileDescriptor* fd, stat_t* st) {
    st.st_mode = S_IFCHR | S_IROTH | S_IRGRP | S_IRUSR;
    return 0;
}
