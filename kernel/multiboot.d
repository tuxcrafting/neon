module kernel.multiboot;

enum MultibootFramebufferType : ubyte {
    Indexed = 0,
    RGB,
    EGAText,
}

struct MultibootInfo {
align(1):
    uint flags;
    uint memLower;
    uint memUpper;
    uint bootDevice;
    uint cmdline;
    uint modsCount;
    uint modsAddr;
    union {
        MultibootAoutSymbolTable aoutSym;
        MultibootELFSectionHeaderTable elfSec;
    }
    uint mmapLength;
    uint mmapAddr;
    uint drivesLength;
    uint drivesAddr;
    uint bootloaderName;
    uint apmTable;
    uint vbeControlInfo;
    uint modeInfo;
    uint mode;
    uint interfaceSeg;
    uint interfaceOff;
    uint interfaceLen;
    ulong framebufferAddr;
    uint framebufferPitch;
    uint framebufferWidth;
    uint framebufferHeight;
    ubyte framebufferBpp;
    MultibootFramebufferType framebufferType;
    union {
        struct {
            uint framebufferPaletteAddr;
            ushort framebufferPaletteNumColors;
        };
        struct {
            ubyte framebufferRedFieldPosition;
            ubyte framebufferRedMaskSize;
            ubyte framebufferGreenFieldPosition;
            ubyte framebufferGreenMaskSize;
            ubyte framebufferBlueFieldPosition;
            ubyte framebufferBlueMaskSize;
        };
    };
}

struct MultibootAoutSymbolTable {
align(1):
    uint tabsize;
    uint strsize;
    uint addr;
    uint reserved;
}

struct MultibootELFSectionHeaderTable {
align(1):
    uint num;
    uint size;
    uint addr;
    uint shndx;
}

struct MultibootColor {
align(1):
    ubyte red;
    ubyte green;
    ubyte blue;
}

enum MultibootMemoryType : uint {
    Available = 1,
    Reserved,
    ACPIReclaimable,
    NVS,
    BadRAM,
}

struct MultibootMmapEntry {
align(1):
    uint size;
    ulong addr;
    ulong length;
    MultibootMemoryType type;
}

struct MultibootModList {
align(1):
    uint modStart;
    uint modEnd;
    uint cmdline;
    uint pad;
}

struct MultibootAPMInfo {
align(1):
    ushort ver;
    ushort cseg;
    uint offset;
    ushort cseg16;
    ushort dseg;
    ushort flags;
    ushort csegLen;
    ushort cseg16Len;
    ushort dsegLen;
}
