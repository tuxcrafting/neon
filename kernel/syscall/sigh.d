module kernel.syscall.sigh;

import kernel.config;
import kernel.errno;
import kernel.main;
import kernel.memory;
import kernel.process;
import kernel.scheduler;
import kernel.signal;
import kernel.types;

@nogc:
nothrow:

int syscallSigaction(int sig, sigaction_t* act, sigaction_t* oact) {
    if (sig < 1 || sig > 27) {
        return -EINVAL;
    }
    if (oact != null) {
        if (!isValidRange(cprocess.cr3,
                          cast(size_t)oact, sigaction_t.sizeof,
                          0b111, 0b111)) {
            return -EFAULT;
        }
        memcpy(oact, &cprocess.signals[sig], sigaction_t.sizeof);
    }
    if (act != null) {
        if (sig == SIGKILL || sig == SIGSTOP) {
            return -EINVAL;
        }
        if (!isValidRange(cprocess.cr3,
                          cast(size_t)act, sigaction_t.sizeof,
                          0b101, 0b101)) {
            return -EFAULT;
        }
        memcpy(&cprocess.signals[sig], act, sigaction_t.sizeof);
    }
    return 0;
}

int syscallKill(pid_t pid, int sig) {
    if (sig < 1 || sig > 27) {
        return -EINVAL;
    }
    if (pid <= 0
     || pid >= MAX_PID
     || processes[pid].state != ProcessState.Running) {
        return -ESRCH;
    }
    if (pid == currentPid) {
        cprocess.saveFrom(&syscallHandler_r);
    }
    if (processes[pid].kill(sig) == 1 && pid == currentPid) {
        switchOrSchedule;
    }
    return 0;
}
