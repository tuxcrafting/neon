module kernel.syscall.all;

public import kernel.syscall.pctl;
public import kernel.syscall.fctl;
public import kernel.syscall.sigh;
public import kernel.syscall.mman;
public import kernel.syscall.nspc;
