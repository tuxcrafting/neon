module kernel.syscall.nspc;

import kernel.memory;
import kernel.process;
import kernel.signal;
import kernel.types;

@nogc:
nothrow:

void syscallSigreturn() {
    memcpy(cprocess,
           &cprocess.sigcontext.uc_mcontext,
           mcontext_t.sizeof);
    cprocess.switchTo;
}
