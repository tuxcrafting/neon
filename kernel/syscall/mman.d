module kernel.syscall.mman;

import kernel.errno;
import kernel.config;
import kernel.fd;
import kernel.process;
import kernel.random;
import kernel.mman;
import kernel.types;

@nogc:
nothrow:

__gshared FileDescriptor* tmpfd;

void* syscallMmap(
        void* addr,
        size_t length,
        int prot,
        int flags,
        int fd,
        off_t offset) {
    if (length == 0) {
        return cast(void*)-EINVAL;
    }
    if (addr == null) {
        addr = cast(void*)(rand & 0x00007FFFFFFFF000);
    }
    size_t iaddr = cast(size_t)addr;
    if ((flags & MAP_FIXED) && iaddr % 4096 != 0) {
        return cast(void*)-EINVAL;
    }
    if ((flags & MAP_FIXED)
     && !isNotValidRange(cprocess.cr3, iaddr, length, PAGE_P, PAGE_P)) {
        return cast(void*)-EEXIST;
    }
    iaddr &= ~4095;
    FileDescriptor* fdp;
    if (tmpfd != null) {
        fdp = tmpfd;
        tmpfd = null;
    } else if (fd == MAPFD_ANON) {
        fdp = null;
    } else {
        if (fd < 0 || fd >= MAX_FD_PER_PID) {
            return cast(void*)-EBADF;
        }
        fdp = cprocess.fds[fd];
    }
    MemMapping* m = mmap(length, prot, flags, fdp, offset);
    if (m == null) {
        return cast(void*)-ENOMEM;
    }
    int pi = -1;
    for (int i = 0; i < MAX_MMAP_PER_PID; i++) {
        if (cprocess.mmaps[i].mmap == null) {
            cprocess.mmaps[i].mmap = m;
            cprocess.mmaps[i].base = addr;
            pi = i;
            break;
        }
    }
    if (pi == -1) {
        munmap(m, length);
        return cast(void*)-EMFILE;
    }
    if (length % 4096 != 0) {
        length += 4096 - length % 4096;
    }
    for (size_t i = 0; i < 0x0000800000000000; i += 4096) {
        if (iaddr >= 0x0000800000000000) {
            iaddr = 0;
        }
        if (isNotValidRange(cprocess.cr3, iaddr, length, PAGE_P, PAGE_P)) {
            addr = pageMmap(
                cprocess,
                iaddr,
                length,
                m,
                prot,
                &cprocess.mmaps[i]);
            if (cast(ssize_t)addr < 0) {
                munmap(m, length);
            }
            return addr;
        }
        iaddr += 4096;
    }
    munmap(m, length);
    return cast(void*)-ENOMEM;
}

int syscallMunmap(void* addr, size_t length) {
    for (size_t i = 0; i < MAX_MMAP_PER_PID; i++) {
        if (cprocess.mmaps[i].base != null
         && cprocess.mmaps[i].base == addr) {
            return munmap(cprocess.mmaps[i].mmap, length);
        }
    }
    return -EINVAL;
}
