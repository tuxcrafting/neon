module kernel.syscall.pctl;

import kernel.config;
import kernel.elf;
import kernel.errno;
import kernel.fcntl;
import kernel.fd;
import kernel.memory;
import kernel.mman;
import kernel.process;
import kernel.scheduler;
import kernel.signal;
import kernel.stat;
import kernel.types;
import kernel.util;
import kernel.wait;

@nogc:
nothrow:

extern(C) extern __gshared ulong[512] p_pml4;

void syscallExit(int status) {
    cprocess.status = status;
    cprocess.destroy;
    schedule;
}

pid_t syscallGetpid() {
    return currentPid;
}

pid_t syscallFork() {
    pid_t child = findPid;
    if (child == -1) {
        return -EAGAIN;
    }
    if (cprocess.forkTo(&processes[child]) != 0) {
        processes[child].destroy;
        return -EAGAIN;
    }
    processes[child].parent = currentPid;
    processes[child].state = ProcessState.Running;
    processes[child].rax = 0;
    queueMain.enqueue(child);
    return child;
}

int syscallWaitid(idtype_t idtype, id_t id, siginfo_t* info, int options) {
    if ((options & (WEXITED | WCONTINUED | WSTOPPED)) == 0) {
        return -EINVAL;
    }
    if (idtype < 0 || idtype > P_ALL) {
        return -EINVAL;
    }
    if (idtype != P_ALL && (id < 1 || id >= MAX_PID)) {
        return -EINVAL;
    }
    if (cprocess.cnum == 0) {
        return -ECHILD;
    }
    cprocess.waitIdType = idtype;
    cprocess.waitId = id;
    cprocess.waitInfo = info;
    cprocess.waitOptions = options;
    cprocess.state = ProcessState.Waiting;
    cprocess.waitReason = WaitReason.WaitID;
    for (pid_t i = 0; i < MAX_PID; i++) {
        if (cprocess.waitIdMatch(&processes[i])) {
            cprocess.triggerSIGCHLD(&processes[i]);
            return 0;
        }
    }
    if (!(options & WNOHANG)) {
        schedule;
    }
    return 0;
}

void xFree(char** buf, size_t blen) {
    for (size_t i = 0; i < blen; i++) {
        if (buf[i] != null) {
            free(buf[i]);
        }
    }
    free(buf);
}

long checkAndCopy(char** orig, char*** buf) {
    size_t len = 0;
    for (len = 0;; len++) {
        if (!bypassFaultCheck) {
            if (!isValidRange(
                    cprocess.cr3,
                    cast(size_t)&orig[len],
                    (char*).sizeof,
                    PAGE_UP,
                    PAGE_UP)) {
                return -EFAULT;
            }
        }
        if (orig[len] == null) {
            break;
        }
    }
    size_t blen = len + 1;
    *buf = cast(char**)calloc(blen * (char*).sizeof);
    if (*buf == null) {
        return -ENOMEM;
    }
    for (size_t i = 0; i < blen - 1; i++) {
        for (len = 0;; len++) {
            if (!bypassFaultCheck) {
                if (!isValidAddress(
                        cprocess.cr3,
                        cast(size_t)&orig[i][len],
                        PAGE_UP,
                        PAGE_UP)) {
                    xFree(*buf, blen);
                    return -EFAULT;
                }
            }
            if (orig[i][len] == '\0') {
                break;
            }
        }
        len++;
        (*buf)[i] = cast(char*)malloc(len);
        if ((*buf)[i] == null) {
            xFree(*buf, blen);
        }
        memcpy((*buf)[i], orig[i], len);
    }
    return blen;
}

int createArrBuffer(char** arr, size_t arrlen) {
    size_t len = 0;
    for (size_t i = 0; i < arrlen - 1; i++) {
        len += strlen(arr[i]) + 1;
    }
    char* b, bp;
    if (len != 0) {
        b = cast(char*)sysMmap(
            cprocess,
            false,
            null,
            len,
            PROT_READ,
            0,
            MAPFD_ANON,
            0);
        if (cast(ssize_t)b < 0) {
            return 0;
        }
        bp =
            cast(char*)getVMA(getPhysicalMapping(cprocess.cr3, cast(size_t)b));
    }
    size_t index = 0;
    cprocess.rsp -= (char*).sizeof * arrlen;
    for (size_t i = 0; i < arrlen; i++) {
        if (arr[i] != null) {
            *(cast(char**)getVMA(
                    getPhysicalMapping(cprocess.cr3, cprocess.rsp)))
                = b + index;
            size_t l = strlen(arr[i]) + 1;
            memcpy(bp + index, arr[i], l);
            index += l;
        } else {
            *(cast(char**)getVMA(
                    getPhysicalMapping(cprocess.cr3, cprocess.rsp)))
                = null;
        }
        cprocess.rsp += (char*).sizeof;
    }
    cprocess.rsp -= (char*).sizeof * arrlen;
    return 1;
}

int syscallFexecve(int fd, char** argv, char** envp) {
    if (fd < 0 || fd >= MAX_FD_PER_PID) {
        return -EBADF;
    }
    FileDescriptor* fdp = cprocess.fds[fd];
    if (fdp == null) {
        return -EBADF;
    }
    if (!(fdp.options & O_RDONLY)) {
        return -EINVAL;
    }
    stat_t st;
    (*fdp).seek(fdp, 0, SEEK_SET);
    (*fdp).stat(fdp, &st);
    ubyte* ebuffer = cast(ubyte*)malloc(st.st_size);
    if (ebuffer == null) {
        return -ENOMEM;
    }
    (*fdp).read(fdp, ebuffer, st.st_size);
    char** argbuf;
    long arglen = checkAndCopy(argv, &argbuf);
    if (arglen < 0) {
        free(ebuffer);
        return cast(int)arglen;
    }
    char** envbuf;
    long envlen = checkAndCopy(envp, &envbuf);
    if (envlen < 0) {
        free(ebuffer);
        xFree(argbuf, arglen);
        return cast(int)arglen;
    }
    for (int i = 0; i < MAX_FD_PER_PID; i++) {
        FileDescriptor* f = cprocess.fds[i];
        if (f != null) {
            if (f.options & O_CLOEXEC) {
                f.safeClose;
                cprocess.fds[i] = null;
            }
        }
    }
    for (int i = 0; i < MAX_MMAP_PER_PID; i++) {
        MemMappingPtr* m = &cprocess.mmaps[i];
        if (m.mmap != null) {
            if (m.base != cast(void*)KSMS_BASE) {
                munmap(m.mmap, m.mmap.length);
                m.mmap = null;
            }
        }
    }
    clearPageTable(cast(ulong*)cprocess.cr3, 3);
    ulong* pml4 = cast(ulong*)_calloc(4096, 4096);
    if (pml4 == null) {
        xFree(argbuf, arglen);
        xFree(envbuf, envlen);
        free(ebuffer);
        return -ENOMEM;
    }
    memcpy(getVMA(pml4), p_pml4.ptr, 4096);
    cprocess.cr3 = cast(ulong)pml4;
    if (loadElf64(cprocess, ebuffer) != 0) {
        xFree(argbuf, arglen);
        xFree(envbuf, envlen);
        free(ebuffer);
        clearPageTable(pml4, 3);
        return -ENOMEM;
    }
    free(ebuffer);
    if (createArrBuffer(envbuf, envlen) == 0) {
        xFree(envbuf, envlen);
        xFree(argbuf, arglen);
        return -ENOMEM;
    }
    xFree(envbuf, envlen);
    if (createArrBuffer(argbuf, arglen) == 0) {
        xFree(argbuf, arglen);
        return -ENOMEM;
    }
    xFree(argbuf, arglen);
    cprocess.rsp -= (char*).sizeof;
    *(cast(size_t*)getVMA(getPhysicalMapping(cprocess.cr3, cprocess.rsp)))
        = arglen - 1;
    bypassFaultCheck = false;
    cprocess.switchTo;
    return 0;
}
