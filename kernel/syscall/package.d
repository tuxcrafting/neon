module kernel.syscall;

// syscall number is stored in RAX
// arguments are stored in RDI, RSI, RDX, R8, R9, R10, R11
// return code is in RAX, and RDX if the return value is 128-bit

enum {
    // process control
    SYSCALLF_PCTL = 0x000,
    // file control
    SYSCALLF_FCTL = 0x100,
    // signal handling
    SYSCALLF_SIGH = 0x200,
    // memory management
    SYSCALLF_MMAN = 0x300,
    // neon-specific
    SYSCALLF_NSPC = 0xF00,
}

enum {
    // void exit(int status)
    SYSCALL_EXIT = SYSCALLF_PCTL + 0,
    // pid_t getpid();
    SYSCALL_GETPID = SYSCALLF_PCTL + 1,
    // pid_t fork();
    SYSCALL_FORK = SYSCALLF_PCTL + 2,
    // int waitid(idtype_t idtype, id_t id, siginfo_t* info, int options);
    SYSCALL_WAITID = SYSCALLF_PCTL + 3,
    // int fexecve(int fd, char** argv, char** envp);
    SYSCALL_FEXECVE = SYSCALLF_PCTL + 4,

    // int openat(int dirfd, char* path, size_t pathlen, int flags, mode_t mode);
    SYSCALL_OPENAT = SYSCALLF_FCTL + 0,
    // int close(int fd);
    SYSCALL_CLOSE = SYSCALLF_FCTL + 1,
    // ssize_t write(int fd, void* buf, size_t length);
    SYSCALL_WRITE = SYSCALLF_FCTL + 2,
    // ssize_t read(int fd, void* buf, long length);
    SYSCALL_READ = SYSCALLF_FCTL + 3,
    // off_t lseek(int fd, off_t offset, int whence);
    SYSCALL_LSEEK = SYSCALLF_FCTL + 4,
    // int fstat(int fd, stat_t* stat);
    SYSCALL_FSTAT = SYSCALLF_FCTL + 5,

    // int sigaction(int sig, sigaction_t* act, sigaction_t* oact);
    SYSCALL_SIGACTION = SYSCALLF_SIGH + 0,
    // int kill(pid_t pid, int sig);
    SYSCALL_KILL = SYSCALLF_SIGH + 1,

    // void* mmap(void* addr, size_t length, int prot, int flags, int fd, off_t offset);
    SYSCALL_MMAP = SYSCALLF_MMAN + 0,
    // int munmap(void* addr, size_t length);
    SYSCALL_MUNMAP = SYSCALLF_MMAN + 1,

    // void sigreturn();
    // NONSTANDARD: cleans up stack after a signal handler. never returns
    SYSCALL_SIGRETURN = SYSCALLF_NSPC + 0,
}
