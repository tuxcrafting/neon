module kernel.syscall.fctl;

import kernel.config;
import kernel.errno;
import kernel.fcntl;
import kernel.fd;
import kernel.fs;
import kernel.memory;
import kernel.path;
import kernel.process;
import kernel.stat;
import kernel.types;
import kernel.util;

@nogc:
nothrow:

int syscallOpenat(
        int dirfd, char* path, size_t pathlen, int flags, mode_t mode) {
    if (pathlen > MAX_PATH_LENGTH) {
        return -ENAMETOOLONG;
    }
    if ((flags & O_ACCMODE) == 0) {
        return -EINVAL;
    }
    if (bypassFaultCheck) {
        bypassFaultCheck = false;
    } else if (!isValidRange(cprocess.cr3,
                      cast(size_t)path, pathlen,
                      PAGE_UP, PAGE_UP) || pathlen == 0) {
        return -EFAULT;
    }
    size_t buflen;
    char[MAX_PATH_LENGTH] buf;
    int atr = getAt(
        cprocess, dirfd, buf.ptr, &buflen, mkstring(path, pathlen));
    if (atr < 0) {
        return atr;
    }
    FileDescriptor* fd = allocGfd;
    if (fd == null) {
        return -ENOMEM;
    }
    int pfd = cprocess.findFd;
    if (pfd == -1) {
        return -EMFILE;
    }
    int r = open(fd, mkstring(buf.ptr, buflen), flags, mode, true);
    if (r < 0) {
        return r;
    }
    cprocess.fds[pfd] = fd;
    if (fd.seek != null) {
        fd.seek(fd, 0, SEEK_SET);
    }
    return pfd;
}

int syscallClose(int fd) {
    if (fd < 0 || fd >= MAX_FD_PER_PID) {
        return -EBADF;
    }
    FileDescriptor* fdp = cprocess.fds[fd];
    if (fdp == null) {
        return -EBADF;
    }
    cprocess.fds[fd] = null;
    return fdp.safeClose;
}

ssize_t syscallWrite(int fd, void* buf, size_t length) {
    if (!isValidRange(cprocess.cr3,
                      cast(size_t)buf, length,
                      PAGE_UP, PAGE_UP)) {
        return -EFAULT;
    }
    if (fd < 0 || fd >= MAX_FD_PER_PID) {
        return -EBADF;
    }
    FileDescriptor* fdp = cprocess.fds[fd];
    if (fdp == null) {
        return -EBADF;
    }
    if (fdp.write == null) {
        return -EINVAL;
    }
    if ((fdp.options & O_APPEND) && fdp.seek != null) {
        fdp.seek(fdp, 0, SEEK_END);
    }
    return fdp.write(fdp, buf, length);
}

ssize_t syscallRead(int fd, void* buf, size_t length) {
    if (!isValidRange(cprocess.cr3,
                      cast(size_t)buf, length,
                      PAGE_WUP, PAGE_WUP)) {
        return -EFAULT;
    }
    if (fd < 0 || fd >= MAX_FD_PER_PID) {
        return -EBADF;
    }
    FileDescriptor* fdp = cprocess.fds[fd];
    if (fdp == null) {
        return -EBADF;
    }
    if (fdp.read == null) {
        return -EINVAL;
    }
    return fdp.read(fdp, buf, length);
}

off_t syscallLseek(int fd, off_t offset, int whence) {
    if (!(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END)) {
        return -EINVAL;
    }
    if (fd < 0 || fd >= MAX_FD_PER_PID) {
        return -EBADF;
    }
    FileDescriptor* fdp = cprocess.fds[fd];
    if (fdp == null) {
        return -EBADF;
    }
    if (fdp.seek == null) {
        return -EINVAL;
    }
    return fdp.seek(fdp, offset, whence);
}

int syscallFstat(int fd, stat_t* stat) {
    if (!isValidRange(cprocess.cr3,
                      cast(size_t)stat, stat_t.sizeof,
                      PAGE_WUP, PAGE_WUP)) {
        return -EFAULT;
    }
    if (fd < 0 || fd >= MAX_FD_PER_PID) {
        return -EBADF;
    }
    FileDescriptor* fdp = cprocess.fds[fd];
    if (fdp == null) {
        return -EBADF;
    }
    if (fdp.stat == null) {
        return -EINVAL;
    }
    return fdp.stat(fdp, stat);
}
