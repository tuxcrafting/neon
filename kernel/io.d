module kernel.io;

@nogc:
nothrow:

void portOut(ushort port, ubyte data) {
    asm @nogc nothrow {
        mov DX, port;
        mov AL, data;
        out DX, AL;
    }
}

ubyte portIn(ushort port) {
    ubyte data;
    asm @nogc nothrow {
        mov DX, port;
        in AL, DX;
        mov data, AL;
    }
    return data;
}
