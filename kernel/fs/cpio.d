module kernel.fs.cpio;

import kernel.cpio;
import kernel.config;
import kernel.errno;
import kernel.fcntl;
import kernel.fd;
import kernel.memory;
import kernel.stat;
import kernel.types;
import kernel.util;

@nogc:
nothrow:

struct CpioFs {
    ubyte* archive;
}

int cpioOpen(
        void* _cpioFs,
        FileDescriptor* fd,
        kstring fullPath,
        kstring relPath,
        int options,
        mode_t mode) {
    CpioFs* cpioFs = cast(CpioFs*)_cpioFs;
    if ((options & O_WRONLY)
     || (options & O_CREAT)) {
        return -EROFS;
    }
    size_t buflen;
    char[MAX_PATH_LENGTH] buf;
    if (relPath.length > 0) {
        memcpy(buf.ptr, relPath.ptr, relPath.length);
        buflen = relPath.length;
    } else {
        buf[0] = '.';
        buflen = 1;
    }
    CpioOdcEntry entry;
    if (cpioFindFile(&entry, cpioFs.archive, mkstring(buf.ptr, buflen))) {
        fd.data = malloc(CpioOdcEntry.sizeof);
        if (fd.data == null) {
            return -ENOMEM;
        }
        memcpy(fd.data, &entry, CpioOdcEntry.sizeof);
        fd.read = &cpioRead;
        fd.write = null;
        fd.seek = &cpioSeek;
        fd.stat = &cpioStat;
        fd.close = &cpioClose;
        return 0;
    } else {
        return -ENOENT;
    }
}

ssize_t cpioRead(FileDescriptor* fd, void* buf, size_t length) {
    CpioOdcEntry* entry = cast(CpioOdcEntry*)fd.data;
    if (fd.offset + length > entry.fileSize) {
        length = entry.fileSize - fd.offset;
    }
    memcpy(buf, entry.file + fd.offset, length);
    fd.offset += length;
    return length;
}

off_t cpioSeek(FileDescriptor* fd, off_t offset, int whence) {
    // it's a ftell, so take a shortcut
    if (offset == 0 && whence == SEEK_CUR) {
        return fd.offset;
    }
    CpioOdcEntry* entry = cast(CpioOdcEntry*)fd.data;
    switch (whence) {
    case SEEK_CUR:
        offset += fd.offset;
        break;
    case SEEK_END:
        offset += entry.fileSize;
        break;
    default:
    }
    if (offset < 0 || offset > entry.fileSize) {
        return -EINVAL;
    }
    fd.offset = offset;
    return offset;
}

int cpioStat(FileDescriptor* fd, stat_t* stat) {
    CpioOdcEntry* entry = cast(CpioOdcEntry*)fd.data;
    memset(stat, 0, stat_t.sizeof);
    stat.st_dev = entry.dev;
    stat.st_ino = entry.ino;
    stat.st_mode = entry.mode;
    stat.st_nlink = entry.nlink;
    stat.st_uid = entry.uid;
    stat.st_gid = entry.gid;
    stat.st_rdev = entry.rdev;
    stat.st_size = entry.fileSize;
    stat.st_mtim.tv_sec = entry.mtime;
    return 0;
}

int cpioClose(FileDescriptor* fd) {
    free(fd.data);
    return 0;
}
