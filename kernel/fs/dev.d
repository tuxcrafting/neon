module kernel.fs.dev;

import kernel.config;
import kernel.errno;
import kernel.fd;
import kernel.memory;
import kernel.stat;
import kernel.types;
import kernel.util;

@nogc:
nothrow:

struct Device {
    bool present;
    @nogc:
    nothrow:
    int function(FileDescriptor*, int, mode_t) open;
    ssize_t function(FileDescriptor*, void*, size_t) write;
    ssize_t function(FileDescriptor*, void*, size_t) read;
    off_t function(FileDescriptor*, off_t, int) seek;
    int function(FileDescriptor*, stat_t*) stat;
    int function(FileDescriptor* fd) close;
}

__gshared Device[512] devices;

int devOpen(
        void* _m,
        FileDescriptor* fd,
        kstring fullPath,
        kstring relPath,
        int options,
        mode_t mode) {
    if (relPath.length == 0) {
        fd.offset = 0;
        fd.read = &devRootRead;
        fd.stat = &devRootStat;
        return 0;
    }
    if (relPath.length != 3) {
        return -ENOENT;
    }
    ssize_t dn = parseOctal(relPath.ptr, 3);
    if (dn < 0 || !devices[dn].present) {
        return -ENOENT;
    }
    Device* dev = &devices[dn];
    fd.write = dev.write;
    fd.read = dev.read;
    fd.seek = dev.seek;
    fd.stat = dev.stat;
    fd.close = dev.close;
    return dev.open(fd, options, mode);
}

ssize_t devRootRead(FileDescriptor* fd, void* buf, size_t length) {
    if (length != DirectoryEntry.sizeof) {
        return -EINVAL;
    }
    if (fd.offset >= 512) {
        return 0;
    }
    DirectoryEntry* entry = cast(DirectoryEntry*)buf;
    entry.ino = 0;
    memset(entry.path.ptr, 0, MAX_PATH_LENGTH);
    entry.path[0] = cast(char)(fd.offset / 64 + 0x30);
    entry.path[1] = cast(char)(fd.offset / 8 % 8 + 0x30);
    entry.path[2] = cast(char)(fd.offset % 8 + 0x30);
    fd.offset++;
    while (!devices[fd.offset].present) {
        fd.offset++;
        if (fd.offset >= 512) {
            break;
        }
    }
    return length;
}

int devRootStat(FileDescriptor* fd, stat_t* st) {
    st.st_mode = S_IFDIR | S_IROTH | S_IRGRP | S_IRUSR;
    return 0;
}
