module kernel.fs;

import kernel.config;
import kernel.errno;
import kernel.fcntl;
import kernel.fd;
import kernel.memory;
import kernel.path;
import kernel.stat;
import kernel.types;

@nogc:
nothrow:

struct MountPoint {
    bool present;
    size_t pathBaseLength;
    ubyte[MAX_PATH_LENGTH] pathBase;
    void* mpdata;

    @nogc:
    nothrow:

    int function(void*, FileDescriptor*, kstring, kstring, int, mode_t) open;
}

__gshared MountPoint[MAX_MOUNT_POINTS] mountPoints;

MountPoint* getMountPoint(kstring path) {
    size_t lpath = 0;
    MountPoint* lmp = null;
    for (size_t i = 0; i < MAX_MOUNT_POINTS; i++) {
        MountPoint* mp = &mountPoints[i];
        if (mp.present
         && path.length >= mp.pathBaseLength
         && mp.pathBaseLength >= lpath
         && memcmp(
                mp.pathBase.ptr,
                path.ptr,
                mp.pathBaseLength) == 0) {
            lpath = mp.pathBaseLength;
            lmp = mp;
        }
    }
    return lmp;
}

int followDirs(
        FileDescriptor* fd,
        kstring path,
        int options,
        mode_t mode,
        stat_t* stat) {
    int ln = 0;
    size_t buflen = path.length;
    char[MAX_PATH_LENGTH] buf;
    memcpy(buf.ptr, path.ptr, path.length);
    size_t mlen = 0;
    size_t start = 0, length = 0;
    nextComponent(mkstring(buf.ptr, buflen), &start, &length);
    for (;;) {
        nextComponent(mkstring(buf.ptr, buflen), &start, &length);
        kstring cp = mkstring(buf.ptr + start, length);
        if (cp.length == 2 && cp.ptr[0] == '.') {
            buflen -= 2;
            memmove(buf.ptr + start, buf.ptr + start + 2, buflen - start);
            start -= 2;
            continue;
        }
        mlen += length;
        int tmpopt = O_RDONLY;
        if (options & O_NOFOLLOW_SUCCESS) {
            if (start + length >= buflen) {
                tmpopt = options;
            }
        } else {
            if (fd.type != FDType.Lnk && start + length >= buflen) {
                tmpopt = options;
            }
        }
        int r = open(fd, mkstring(buf.ptr, mlen), tmpopt, mode, false);
        if (r < 0) {
            return r;
        }
        if (start + length >= buflen) {
            if ((options & O_NOFOLLOW) && fd.type == FDType.Lnk) {
                fd.close(fd);
                return -ELOOP;
            }
            if (((options & O_NOFOLLOW_SUCCESS) && fd.type == FDType.Lnk)
             || (!(options & O_NOFOLLOW_SUCCESS) && fd.type != FDType.Lnk)) {
                return 0;
            }
        }
        if (fd.type == FDType.Lnk) {
            ln++;
            if (ln == MAX_LINKS_FOLLOWED) {
                fd.safeClose;
                return -ELOOP;
            }
            fd.stat(fd, stat);
            if (stat.st_size + buflen > MAX_PATH_LENGTH) {
                fd.safeClose;
                return -ENAMETOOLONG;
            }
            memmove(buf.ptr + stat.st_size, buf.ptr + mlen, buflen - mlen);
            fd.read(fd, buf.ptr, stat.st_size);
            buflen = buflen - mlen + stat.st_size;
            start = length = 0;
            nextComponent(mkstring(buf.ptr, buflen), &start, &length);
            mlen = 0;
        } else if (fd.type != FDType.Dir) {
            fd.safeClose;
            return -ENOTDIR;
        }
        fd.safeClose;
    }
}

FDType modeToType(mode_t mode) {
    mode_t t = mode & S_IFMT;
    if (t == S_IFIFO) {
        return FDType.Fifo;
    } else if (t == S_IFCHR) {
        return FDType.ChrDev;
    } else if (t == S_IFDIR) {
        return FDType.Dir;
    } else if (t == S_IFBLK) {
        return FDType.BlkDev;
    } else if (t == S_IFREG) {
        return FDType.Reg;
    } else if (t == S_IFLNK) {
        return FDType.Lnk;
    } else {
        return FDType.Sock;
    }
}

int open(
        FileDescriptor* fd,
        kstring _path,
        int options,
        mode_t mode,
        bool follow) {
    // opening a directory for writing is invalid
    if ((options & O_WRONLY) && (options & O_DIRECTORY)) {
        return -EINVAL;
    }
    kstring path = mkstring(_path.ptr, _path.length);
    if (path.length > 1 && path.ptr[path.length - 1] == '/') {
        path.length--;
    }
    if (follow) {
        stat_t st;
        int r = followDirs(fd, path, options, mode, &st);
        if (r < 0) {
            return r;
        }
        fd.stat(fd, &st);
        if ((st.st_mode & S_IFMT) != S_IFDIR && (options & O_DIRECTORY)) {
            fd.safeClose;
            return -ENOTDIR;
        }
        return 0;
    } else {
        MountPoint* mp = getMountPoint(path);
        if (mp == null) {
            return -ENOENT;
        }
        memcpy(fd.path.ptr, path.ptr, path.length);
        fd.pathLength = path.length;
        kstring rpath = mkstring(
            path.ptr + mp.pathBaseLength,
            path.length - mp.pathBaseLength);
        if (rpath.length > 0 && rpath.ptr[0] == '/') {
            rpath.length--;
            rpath.ptr++;
        }
        int r = mp.open(
            mp.mpdata,
            fd,
            path,
            rpath,
            options,
            mode);
        if (r < 0) {
            return r;
        }
        stat_t stat;
        fd.stat(fd, &stat);
        fd.type = modeToType(stat.st_mode);
        fd.options = options;
    }
    fd.refcnt++;
    return 0;
}
