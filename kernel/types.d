module kernel.types;

@nogc:
nothrow:

alias ulong size_t;
alias long ssize_t;
alias int id_t;
alias id_t pid_t;
alias id_t uid_t;
alias id_t gid_t;
alias int idtype_t;
alias uint sigset_t;
alias int dev_t;
alias size_t ino_t;
alias int nlink_t;
alias int mode_t;
alias long off_t;
alias ulong blksize_t;
alias ulong blkcnt_t;
alias long time_t;
alias ulong hash_t;

struct timespec_t {
    time_t tv_sec;
    long tv_nsec;
}

struct kstring {
    size_t length;
    char* ptr;

    @nogc:
    nothrow:

    char opIndex(size_t i) {
        return ptr[i];
    }

    int opApply(scope int delegate(ref char) @nogc nothrow dg) {
        int result = 0;
        for (size_t i = 0; i < length; i++) {
            result = dg(ptr[i]);
            if (result) {
                break;
            }
        }
        return result;
    }
    int opApply(scope int delegate(ref size_t, ref char) @nogc nothrow dg) {
        int result = 0;
        for (size_t i = 0; i < length; i++) {
            result = dg(i, ptr[i]);
            if (result) {
                break;
            }
        }
        return result;
    }
}

kstring mkstring(char* ptr, size_t length) {
    kstring s;
    s.ptr = ptr;
    s.length = length;
    return s;
}

kstring KS(string s)() {
    return mkstring(cast(char*)s.ptr, s.length);
}
