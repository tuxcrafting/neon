module kernel.util;

import kernel.vga;
import kernel.types;

@nogc:
nothrow:

enum size_t KERNEL_VMA_BASE = 0xFFFF800000000000;
enum ushort SEG_NULL = 0x0000;
enum ushort SEG_CS_KERNEL = 0x0010;
enum ushort SEG_DS_KERNEL = 0x0020;
enum ushort SEG_CS_USER = 0x0030;
enum ushort SEG_DS_USER = 0x0040;
enum ushort SEG_TSS = 0x0050;
// kernel shared memory space
enum size_t KSMS_BASE = 0xFF000000;

__gshared bool bypassFaultCheck;

// workaround for a few nasty bugs in ldc2
U explicitCast(T, U)(T x) {
    return cast(U)x;
}

void examine(T)(T x) {
    asm @nogc nothrow {
        lea RBX, x;
        mov RAX, [RBX];
busy:   cli;
        jmp busy;
    }
}

void* getVMA(void* ptr) {
    return ptr + KERNEL_VMA_BASE;
}

bool isCanonicalAddress(size_t addr) {
    if (addr & 0x0000800000000000) {
        return (addr & 0xFFFF000000000000) == 0xFFFF000000000000;
    } else {
        return (addr & 0xFFFF000000000000) == 0x0000000000000000;
    }
}

void panic(kstring s) {
    colorBg = Color.Red;
    write(KS!"\n\nKernel panic!!\n");
    colorBg = Color.Black;
    write(s);
    examine(0);
}

ssize_t parseOctal(char* buf, size_t length) {
    ssize_t n = 0;
    for (size_t i = 0; i < length; i++) {
        if (buf[i] < '0' || buf[i] > '7') {
            return -1;
        }
        n *= 8;
        n += buf[i] - 0x30;
    }
    return n;
}
