module kernel.fcntl;

import kernel.types;

enum {
    F_DUPFD,
    F_DUPFD_CLOEXEC,
    F_GETFD,
    F_SETFD,
    F_GETFL,
    F_SETFL,
    F_GETLK,
    F_SETLK,
    F_SETLKW,
    F_GETOWN,
    F_SETOWN,
}

enum {
    F_RDLCK,
    F_UNLCK,
    F_WRLCK,
}

enum {
    SEEK_SET = 0,
    SEEK_CUR = 1,
    SEEK_END = -1,
}

enum {
    O_RDONLY = 0x00000001,
    O_WRONLY = 0x00000002,
    O_RDWR = 0x00000003,
    O_ACCMODE = 0x00000003,

    O_CLOEXEC = 0x00000004,
    O_CREAT = 0x00000008,
    O_DIRECTORY = 0x00000010,
    O_EXCL = 0x00000020,
    O_NOCTTY = 0x00000040,
    O_NOFOLLOW = 0x00000080,
    O_TRUNC = 0x00000100,
    O_TTY_INIT = 0x00000200,
    O_APPEND = 0x00000400,
    O_SYNC = 0x00000800,
    O_NONBLOCK = 0x00001000,

    O_NOFOLLOW_SUCCESS = 0x00100000,
}

enum {
    AT_FDCWD = -1,
}

enum {
    AT_SYMLINK_NOFOLLOW = 1,
    AT_SYMLINK_FOLLOW = 1,
    AT_REMOVEDIR = 1,
}

enum {
    POSIX_FADV_DONTNEED,
    POSIX_FADV_NOREUSE,
    POSIX_FADV_NORMAL,
    POSIX_FADV_RANDOM,
    POSIX_FADV_SEQUENTIAL,
    POSIX_FADV_WILLNEED,
}

struct flock_t {
    short l_type;
    short l_whence;
    off_t l_start;
    off_t l_len;
    pid_t l_pid;
}
