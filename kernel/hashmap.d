module kernel.hashmap;

import kernel.memory;
import kernel.types;

@nogc:
nothrow:

hash_t hash(kstring s) {
    hash_t hash = 14695981039346656037UL;
    foreach (char c; s) {
        hash *= 1099511628211UL;
        hash ^= c;
    }
    return hash;
}

hash_t hash(long n) {
    return n;
}

union HashMapElement(K, V) {
    HashMap!(K, V)* map;
    struct {
        K key;
        V val;
    }
}

// 64-bit hashes
// 16 (2^4) elements per level
// 16 (64/4) levels
struct HashMap(K, V) {
    @nogc:
    nothrow:

    void del(K key) {
        HashMapElement!(K, V)** elm = find(key, 15, false);
        if (*elm != null) {
            (*elm).destroy;
            free(*elm);
            *elm = null;
            cleanup(15);
        }
    }

    V opIndex(K key) {
        HashMapElement!(K, V)* elm = *find(key.hash, 15, false);
        if (elm == null) {
            return cast(V)null;
        }
        return elm.val;
    }

    V opIndexAssign(V val, K key) {
        HashMapElement!(K, V)* elm = *find(key.hash, 15, true);
        if (elm == null) {
            return cast(V)null;
        }
        elm.key = key;
        return elm.val = val;
    }

    int opApply(scope int delegate(ref K, ref V) @nogc nothrow dg) {
        return walk(15, dg);
    }

    ~this() {
        rfree(15);
    }

private:
    HashMapElement!(K, V)*[16] branches;

    HashMapElement!(K, V)** find(hash_t key, int level, bool create) {
        HashMapElement!(K, V)** elm = &branches[key & 15];
        if (*elm == null) {
            if (create) {
                *elm = cast(HashMapElement!(K, V)*)
                    calloc(HashMapElement!(K, V).sizeof);
            } else {
                return null;
            }
        }
        if (level == 0) {
            return elm;
        }
        if ((*elm).map == null) {
            if (create) {
                (*elm).map = cast(HashMap!(K, V)*)
                    calloc(HashMap!(K, V).sizeof);
                if ((*elm).map == null) {
                    return null;
                }
            } else {
                return null;
            }
        }
        return (*elm).map.find(key >> 4, level - 1, create);
    }

    int walk(int level, scope int delegate(ref K, ref V) @nogc nothrow dg) {
        int result;
        for (int i = 0; i < 16; i++) {
            HashMapElement!(K, V)* elm = branches[i];
            if (elm == null) {
                continue;
            }
            if (level == 0) {
                if (elm.val != cast(V)null) {
                    result = dg(elm.key, elm.val);
                }
            } else {
                if (elm.map != null) {
                    result = elm.map.walk(level - 1, dg);
                }
            }
            if (result) {
                break;
            }
        }
        return result;
    }

    void rfree(int level) {
        for (int i = 0; i < 16; i++) {
            HashMapElement!(K, V)* elm = branches[i];
            if (elm == null) {
                continue;
            }
            if (level != 0) {
                if (elm.map != null) {
                    elm.map.rfree(level - 1);
                    free(elm.map);
                }
            }
            free(elm);
        }
    }

    int cleanup(int level) {
        int result;
        for (int i = 0; i < 16; i++) {
            HashMapElement!(K, V)** elm = &branches[i];
            if (*elm == null) {
                continue;
            }
            result = 1;
            if (level != 0 && (*elm).map != null) {
                int r = (*elm).map.cleanup(level - 1);
                if (r == 0) {
                    free((*elm).map);
                    free(*elm);
                    *elm = null;
                }
            }
        }
        return result;
    }
}
