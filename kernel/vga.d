module kernel.vga;

import kernel.errno;
import kernel.fcntl;
import kernel.fd;
import kernel.io;
import kernel.util;
import kernel.stat;
import kernel.types;

@nogc:
nothrow:

enum Color {
    Black = 0,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Brown,
    Gray,
    DarkGray,
    BrightBlue,
    BrightGreen,
    BrightCyan,
    BrightRed,
    BrightMagenta,
    Yellow,
    White,
}

__gshared long textX = 0;
__gshared long textY = 0;

__gshared Color colorFg = Color.Gray;
__gshared Color colorBg = Color.Black;

enum long MAX_COLUMNS = 80;
enum long MAX_ROWS = 25;

enum ushort *VGA_BASE = cast(ushort*)(0xB8000 + KERNEL_VMA_BASE);

void cursorEnable(ubyte cursorStart, ubyte cursorEnd) {
    portOut(0x3D4, 0x0A);
    portOut(0x3D5, (portIn(0x3D5) & 0xC0) | cursorStart);
    portOut(0x3D4, 0x0B);
    portOut(0x3D5, (portIn(0x3E0) & 0xE0) | cursorEnd);
}

void cursorDisable() {
    portOut(0x3D4, 0x0A);
    portOut(0x3D5, 0x20);
}

void cursorUpdate() {
    long pos = textY * MAX_COLUMNS + textX;
    portOut(0x3D4, 0x0F);
    portOut(0x3D5, cast(ubyte)(pos & 0xFF));
    portOut(0x3D4, 0x0E);
    portOut(0x3D5, cast(ubyte)((pos >> 8) & 0xFF));
}

void clear() {
    for (long i = 0; i < MAX_COLUMNS * MAX_ROWS; i++) {
        VGA_BASE[i] = 0x0720;
    }
}

void scroll(long n) {
    long delta = n * MAX_COLUMNS;
    for (long i = 0; i < MAX_COLUMNS * MAX_ROWS - delta; i++) {
        VGA_BASE[i] = VGA_BASE[i + delta];
    }
    textY -= n;
    if (textY < 0) {
        textY = 0;
    }
}

void write(char c) {
    switch (c) {
    case '\0':
        return;
    case '\x08':
        if (textX > 0) {
            textX--;
        }
        VGA_BASE[textY * MAX_COLUMNS + textX] =
            cast(ushort)(0 | (colorFg << 8) | (colorBg << 12));
        break;
    case '\r':
        textX = 0;
        break;
    case '\n':
        textX = 0;
        textY++;
        break;
    default:
        VGA_BASE[textY * MAX_COLUMNS + textX] =
            cast(ushort)(c | (colorFg << 8) | (colorBg << 12));
        textX++;
    }
    if (textX == MAX_COLUMNS - 1) {
        textX = 0;
        textY++;
    }
    if (textY == MAX_ROWS - 1) {
        scroll(1);
    }
    cursorUpdate;
}

void write(kstring s) {
    foreach (char c; s) {
        write(c);
    }
}

void writeRaw(char* s, size_t length) {
    for (size_t i = 0; i < length; i++) {
        write(s[i]);
    }
}

int vgaDevOpen(FileDescriptor* fd, int options, mode_t mode) {
    if (options & O_RDONLY) {
        return -EINVAL;
    }
    return 0;
}
ssize_t vgaDevWrite(FileDescriptor* fd, void* buf, size_t length) {
    writeRaw(cast(char*)buf, length);
    return length;
}
int vgaDevStat(FileDescriptor* fd, stat_t* st) {
    st.st_mode = S_IFCHR | S_IWOTH | S_IWGRP | S_IWUSR;
    return 0;
}
