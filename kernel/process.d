module kernel.process;

import kernel.config;
import kernel.errno;
import kernel.fd;
import kernel.interrupt;
import kernel.memory;
import kernel.mman;
import kernel.signal;
import kernel.syscall;
import kernel.syscall.mman;
import kernel.types;
import kernel.util;
import kernel.wait;

@nogc:
nothrow:

extern(C) extern __gshared ulong[512] p_pml4;

__gshared Process[MAX_PID] processes;
__gshared ProcessGroup[MAX_PID] pgroups;
__gshared Session[MAX_PID] sessions;
__gshared pid_t nextPid, currentPid;
__gshared Process* cprocess;

struct Session {
    pid_t pgnum;
}

struct ProcessGroup {
    pid_t session;
    pid_t leader;
    pid_t pnum;
}

enum ProcessState {
    Claimable,
    Running,
    Zombie,
    Waiting,
}

enum WaitReason {
    Other,
    WaitID,
    Signal,
}

struct Process {
    ulong rax, rbx, rcx, rdx, rsi, rdi, rbp, rsp,
           r8,  r9, r10, r11, r12, r13, r14, r15;
    ulong rip, rflags;
    ulong cr3;

    int status;

    uid_t uid;
    pid_t pgid;
    pid_t parent;

    ProcessState state;
    int flags;
    sigaction_t[28] signals;
    ucontext_t sigcontext;
    siginfo_t siginfo;
    pid_t cnum;

    FileDescriptor*[MAX_FD_PER_PID] fds;

    size_t cwdLength;
    ubyte[MAX_PATH_LENGTH] cwd;

    void* ksmsBase;

    WaitReason waitReason;
    idtype_t waitIdType;
    id_t waitId;
    siginfo_t* waitInfo;
    int waitOptions;

    MemMappingPtr[MAX_MMAP_PER_PID] mmaps;

    pid_t pid;

    @nogc:
    nothrow:

    void switchTo() {
        if (state != ProcessState.Running) {
            return;
        }
        Process* thisAddr = &this;
        asm @nogc nothrow {
            mov AX, SEG_DS_USER + 3;
            mov DS, AX;
            mov ES, AX;
            mov FS, AX;
            mov GS, AX;
            mov RBX, thisAddr;
            push SEG_DS_USER + 3;
            push Process.rsp.offsetof[RBX];
            push Process.rflags.offsetof[RBX];
            push SEG_CS_USER + 3;
            push Process.rip.offsetof[RBX];
            mov RAX, Process.cr3.offsetof[RBX];
            mov CR3, RAX;
            mov RAX, Process.rax.offsetof[RBX];
            mov RCX, Process.rcx.offsetof[RBX];
            mov RDX, Process.rdx.offsetof[RBX];
            mov RSI, Process.rsi.offsetof[RBX];
            mov RDI, Process.rdi.offsetof[RBX];
            mov RBP, Process.rbp.offsetof[RBX];
            mov R8, Process.r8.offsetof[RBX];
            mov R9, Process.r9.offsetof[RBX];
            mov R10, Process.r10.offsetof[RBX];
            mov R11, Process.r11.offsetof[RBX];
            mov R12, Process.r12.offsetof[RBX];
            mov R13, Process.r13.offsetof[RBX];
            mov R14, Process.r14.offsetof[RBX];
            mov R15, Process.r15.offsetof[RBX];
            mov RBX, Process.rbx.offsetof[RBX];
            iretq;
        }
    }

    void saveFrom(Registers* regs) {
        memcpy(&this, regs, 8 * 18);
    }

    void destroy() {
        if (pid == 1) {
            panic(KS!"Attempted to kill init");
        }
        state = ProcessState.Zombie;
        if (parent == 0) {
            return;
        }
        ProcessGroup* pg = &pgroups[pgid];
        if (pg.leader == pid) {
            pg.leader = 0;
        }
        if (--pg.pnum == 0) {
            Session* s = &sessions[pg.session];
            s.pgnum--;
        }
        for (pid_t i = 0, j = 0; i < MAX_PID && j < cnum; i++) {
            if (processes[i].parent == pid) {
                processes[i].parent = 1;
                processes[1].cnum++;
                j++;
            }
        }
        for (int i = 0; i < MAX_FD_PER_PID; i++) {
            if (fds[i] != null) {
                fds[i].safeClose;
                fds[i] = null;
            }
        }
        for (int i = 0; i < MAX_MMAP_PER_PID; i++) {
            if (mmaps[i].mmap != null) {
                munmap(mmaps[i].mmap, mmaps[i].mmap.length);
                mmaps[i].mmap = null;
            }
        }
        clearPageTable(cast(ulong*)cr3, 3);
        processes[parent].triggerSIGCHLD(&this);
    }

    void triggerSIGCHLD(Process* child) {
        clearSignalInfo;
        siginfo.si_pid = child.pid;
        siginfo.si_uid = child.uid;
        siginfo.si_status = child.status;
        if (state == ProcessState.Waiting
         && waitReason == WaitReason.WaitID
         && waitIdMatch(child)) {
            if (!(waitOptions & WNOWAIT)) {
                child.state = ProcessState.Claimable;
            }
            state = ProcessState.Running;
            if (isValidRange(
                    cr3, cast(size_t)waitInfo, siginfo_t.sizeof, 7, 7)) {
                void* info = getPhysicalMapping(cr3, cast(ulong)waitInfo);
                memcpy(info, &siginfo, siginfo_t.sizeof);
            }
            cnum--;
            rax = 0;
        } else {
            kill(SIGCHLD);
        }
    }

    void clear() {
        memset(&this, 0, Process.sizeof - pid_t.sizeof);
        rflags = 0x200;
    }

    long init() {
        ulong* pml4 = cast(ulong*)_calloc(4096, 4096);
        if (pml4 == null) {
            return 1;
        }
        memcpy(getVMA(pml4), p_pml4.ptr, 4096);
        cr3 = cast(ulong)pml4;
        void* ksms = sysMmap(
            &this,
            true,
            cast(void*)KSMS_BASE,
            4096,
            PROT_READ | PROT_WRITE | PROT_EXEC,
            MAP_FIXED,
            MAPFD_ANON,
            0);
        if (cast(ssize_t)ksms < 0) {
            return 1;
        }
        void* ksms_vma = getVMA(ksms);
        // movl $SYSCALL_SIGRETURN, %eax
        (cast(ubyte*)ksms_vma)[0] = 0xB8;
        *cast(uint*)(ksms_vma + 1) = SYSCALL_SIGRETURN;
        // int $0x80
        (cast(ubyte*)ksms_vma)[5] = 0xCD;
        (cast(ubyte*)ksms_vma)[6] = 0x80;
        ksmsBase = ksms_vma;
        return 0;
    }

    long forkTo(Process* target) {
        int a = target.pid;
        memcpy(target, &this, Process.sizeof);
        target.pid = a;
        if ((*target).init == 1) {
            return 1;
        }
        for (int i = 0; i < MAX_FD_PER_PID; i++) {
            FileDescriptor* fd = target.fds[i];
            if (fd != null) {
                fd.refcnt++;
            }
        }
        for (int i = 0; i < MAX_MMAP_PER_PID; i++) {
            MemMappingPtr m = mmaps[i];
            if (m.mmap != null) {
                if (cast(size_t)m.base == KSMS_BASE) {
                    continue;
                }
                if (m.mmap.flags & MAP_SHARED) {
                    m.mmap.refcnt++;
                    if (cast(ssize_t)pageMmap(
                            target,
                            cast(size_t)m.base,
                            m.mmap.length,
                            m.mmap,
                            m.mmap.prot,
                            &target.mmaps[i]) < 0) {
                        return 1;
                    }
                } else {
                    target.mmaps[i].mmap = null;
                    tmpfd = m.mmap.fd;
                    void* addr = sysMmap(
                        target,
                        true,
                        m.base,
                        m.mmap.length,
                        m.mmap.prot,
                        m.mmap.flags | MAP_FIXED,
                        MAPFD_ANON,
                        m.mmap.offset);
                    if (cast(ssize_t)addr < 0) {
                        return cast(ssize_t)addr;
                    }
                    if (m.mmap.fd == null) {
                        memcpy(
                            getVMA(addr),
                            getVMA(m.mmap.base),
                            m.mmap.length);
                    }
                }
            }
        }
        cnum++;
        return 0;
    }

    void clearSignalInfo() {
        memset(&siginfo, 0, siginfo_t.sizeof);
        memset(&sigcontext, 0, ucontext_t.sizeof);
    }

    long kill(int sig) {
        sigaction_t* sigaction = &signals[sig];
        if ((sigaction._sa_int == SIG_DFL
          && signalDefaultActions[sig] == __SIG_ACTION_I)
         || sigaction._sa_int == SIG_IGN) {
            return 0;
        } else if (sigaction._sa_int == SIG_DFL) {
            switch (signalDefaultActions[sig]) {
            case __SIG_ACTION_T:
            case __SIG_ACTION_A:
                status = 130;
                destroy;
                return 1;
            case __SIG_ACTION_S:
                state = ProcessState.Waiting;
                waitReason = WaitReason.Signal;
                break;
            case __SIG_ACTION_C:
                state = ProcessState.Running;
                break;
            default:
                return 0;
            }
        }
        if (state == ProcessState.Waiting) {
            if (waitReason != WaitReason.Signal) {
                rax = -EINTR;
            }
            state = ProcessState.Running;
        }
        memcpy(&sigcontext.uc_mcontext, &this, mcontext_t.sizeof);
        rsp -= 8;
        if (!isValidRange(cr3, rsp, 8, 0b111, 0b111)) {
            destroy;
            return 1;
        }
        if (sigaction.sa_flags & SA_RESTORER) {
            *cast(void**)rsp = sigaction.sa_restorer;
        } else {
            *cast(ulong*)rsp = KSMS_BASE;
        }
        rip = cast(ulong)sigaction.sa_handler;
        rdi = sig;
        if (sigaction.sa_flags & SA_SIGINFO) {
            rsi = KSMS_BASE + 0x80;
            rdx = KSMS_BASE + 0x100;
            memcpy(ksmsBase + 0x80, &siginfo, siginfo_t.sizeof);
            memcpy(ksmsBase + 0x100, &sigcontext, ucontext_t.sizeof);
        }
        return 1;
    }

    bool waitIdMatch(Process* process) {
        if (process.parent != pid) {
            return false;
        }
        switch (waitIdType) {
        case P_PID:
            if (process.pid != waitId) {
                return false;
            }
            break;
        case P_PGID:
            if (process.pgid != waitId) {
                return false;
            }
            break;
        default:
        }
        if ((waitOptions & WEXITED) && process.state == ProcessState.Zombie) {
            return true;
        }
        return false;
    }

    int findFd() {
        for (int i = 0; i < MAX_FD_PER_PID; i++) {
            if (fds[i] == null) {
                return i;
            }
        }
        return -1;
    }
}

pid_t findPid() {
    for (pid_t i = nextPid, j = 0; j < MAX_PID - 2; i++, j++) {
        if (i == MAX_PID) {
            i = 2;
        }
        if (processes[i].state == ProcessState.Claimable) {
            if (pgroups[i].pnum > 0) {
                continue;
            }
            if (sessions[i].pgnum > 0) {
                continue;
            }
            nextPid = i + 1;
            if (nextPid < 2) {
                nextPid = 2;
            }
            return i;
        }
    }
    return -1;
}

void* createPage(ulong* pml4, size_t vaddr, ulong flags, void* target=null) {
    void* m;
    ulong* pml4e = &(cast(ulong*)getVMA(pml4))[(vaddr >> 39) & 511];
    if ((*pml4e & 1) == 0) {
        m = _calloc(4096, 4096);
        if (m == null) {
            return null;
        }
        *pml4e = explicitCast!(void*, ulong)(m) + 0x7;
    }
    ulong* pdpe = &(cast(ulong*)getVMA(cast(void*)(*pml4e & ~0xFFF)))
                  [(vaddr >> 30) & 511];
    if ((*pdpe & 1) == 0) {
        m = _calloc(4096, 4096);
        if (m == null) {
            return null;
        }
        *pdpe = explicitCast!(void*, ulong)(m) + 0x7;
    }
    ulong* pde = &(cast(ulong*)getVMA(cast(void*)(*pdpe & ~0xFFF)))
                 [(vaddr >> 21) & 511];
    if ((*pde & 1) == 0) {
        m = _calloc(4096, 4096);
        if (m == null) {
            return null;
        }
        *pde = explicitCast!(void*, ulong)(m) + 0x7;
    }
    ulong* pe = &(cast(ulong*)getVMA(cast(void*)(*pde & ~0xFFF)))
                [(vaddr >> 12) & 511];
    if ((*pe & PAGE_P) == 0) {
        if (target == null) {
            m = _calloc(4096, 4096);
            if (m == null) {
                return null;
            }
            *pe = cast(ulong)m;
        } else {
            *pe = cast(ulong)target;
        }
    }
    *pe |= flags;
    return cast(void*)(*pe & ~0xFFF);
}

void unmapPage(ulong* pml4, size_t vaddr) {
    ulong* pml4e = &(cast(ulong*)getVMA(pml4))[(vaddr >> 39) & 511];
    if ((*pml4e & 1) == 0) {
        return;
    }
    ulong* pdpe = &(cast(ulong*)getVMA(cast(void*)(*pml4e & ~0xFFF)))
                  [(vaddr >> 30) & 511];
    if ((*pdpe & 1) == 0) {
        return;
    }
    ulong* pde = &(cast(ulong*)getVMA(cast(void*)(*pdpe & ~0xFFF)))
                 [(vaddr >> 21) & 511];
    if ((*pde & 1) == 0) {
        return;
    }
    ulong* pe = &(cast(ulong*)getVMA(cast(void*)(*pde & ~0xFFF)))
                [(vaddr >> 12) & 511];
    if ((*pe & PAGE_P) == 1) {
        *pe = 0;
    }
}

void clearPageTable(ulong* pt, long level) {
    for (long i = 0; i < (level == 3 ? 256 : 512); i++) {
        if ((cast(ulong*)getVMA(pt))[i] & 1) {
            if (level == 0) {
                _free(cast(void*)(cast(ulong*)getVMA(pt))[i]);
            } else {
                clearPageTable(cast(ulong*)((cast(ulong*)getVMA(pt))[i]
                    & ~0xFFF), level - 1);
            }
        }
    }
    _free(pt);
}

long duplicatePageTable(ulong* pt, ulong* dpt, long level) {
    for (long i = 0; i < (level == 3 ? 256 : 512); i++) {
        if ((cast(ulong*)getVMA(pt))[i] & 1) {
            ulong page = cast(ulong)_calloc(4096, 4096);
            if (page == 0) {
                return 1;
            }
            (cast(ulong*)getVMA(dpt))[i] = page
                | ((cast(ulong*)getVMA(pt))[i] & 0xFFF);
            if (level > 0) {
                if(duplicatePageTable(
                    cast(ulong*)((cast(ulong*)getVMA(pt))[i] & ~0xFFF),
                    cast(ulong*)((cast(ulong*)getVMA(dpt))[i] & ~0xFFF),
                    level - 1) != 0) {
                    return 1;
                }
            } else {
                memcpy(
                    getVMA(cast(void*)((cast(ulong*)getVMA(dpt))[i] & ~0xFFF)),
                    getVMA(cast(void*)((cast(ulong*)getVMA(pt))[i] & ~0xFFF)),
                    4096);
            }
        }
    }
    return 0;
}

bool isValidAddress(ulong pml4, size_t addr, int mask, int flags) {
    if (!isCanonicalAddress(addr)) {
        return false;
    }
    ulong* pml4e = &(cast(ulong*)getVMA(cast(void*)pml4))
                   [(addr >> 39) & 511];
    if ((*pml4e & 1) != 1) {
        return false;
    }
    ulong* pdpe = &(cast(ulong*)getVMA(cast(void*)(*pml4e & ~0xFFF)))
                  [(addr >> 30) & 511];
    if ((*pdpe & 1) != 1) {
        return false;
    }
    ulong* pde = &(cast(ulong*)getVMA(cast(void*)(*pdpe & ~0xFFF)))
                 [(addr >> 21) & 511];
    if ((*pde & 1) != 1) {
        return false;
    }
    ulong* pe = &(cast(ulong*)getVMA(cast(void*)(*pde & ~0xFFF)))
                [(addr >> 12) & 511];
    if ((*pe & mask) != flags) {
        return false;
    }
    return true;
}

void* getPhysicalMapping(ulong pml4, size_t addr) {
    ulong* pml4e = &(cast(ulong*)getVMA(cast(void*)pml4))
                   [(addr >> 39) & 511];
    if (!(*pml4e & 1)) {
        return null;
    }
    ulong* pdpe = &(cast(ulong*)getVMA(cast(void*)(*pml4e & ~0xFFF)))
                  [(addr >> 30) & 511];
    if (!(*pdpe & 1)) {
        return null;
    }
    ulong* pde = &(cast(ulong*)getVMA(cast(void*)(*pdpe & ~0xFFF)))
                 [(addr >> 21) & 511];
    if (!(*pde & 1)) {
        return null;
    }
    ulong* pe = &(cast(ulong*)getVMA(cast(void*)(*pde & ~0xFFF)))
                [(addr >> 12) & 511];
    if (!(*pe & 1)) {
        return null;
    }
    return cast(void*)((*pe & ~0xFFF) + (addr & 0xFFF));
}

bool isValidRange(
        ulong pml4,
        size_t base,
        size_t length,
        int mask,
        int flags) {
    for (;;) {
        if (!isValidAddress(pml4, base, mask, flags)) {
            return false;
        }
        if (length <= 4096) {
            break;
        }
        length -= 4906;
        base += 4096;
    }
    return true;
}

bool isNotValidRange(
        ulong pml4,
        size_t base,
        size_t length,
        int mask,
        int flags) {
    for (;;) {
        if (isValidAddress(pml4, base, mask, flags)) {
            return false;
        }
        if (length <= 4096) {
            break;
        }
        length -= 4096;
        base += 4096;
    }
    return true;
}

enum {
    PAGE_W = 0b10,
    PAGE_U = 0b100,
    PAGE_P = 1 << 9,
    PAGE_WUP = PAGE_W | PAGE_U | PAGE_P,
    PAGE_UP = PAGE_U | PAGE_P,
}
