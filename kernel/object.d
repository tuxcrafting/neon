module object;

import kernel.types;
import kernel.util;

@nogc:
nothrow:

alias size_t = ulong;
alias ptrdiff_t = long;
alias string = immutable(char)[];

extern(C) void _d_dso_registry() {}
extern(C) void __assert() {}
extern(C) void __assert_fail() {
    panic(KS!"Assertion failed!");
}
