module kernel.mman;

import kernel.config;
import kernel.errno;
import kernel.fd;
import kernel.memory;
import kernel.process;
import kernel.syscall.mman;
import kernel.types;

@nogc:
nothrow:

enum {
    PROT_READ = 1,
    PROT_WRITE = 2,
    PROT_EXEC = 4,
    PROT_NONE = 0,
}

enum {
    // shared = 01
    // private = 11
    // shared | private == private
    // so it's not possible to have an invalid SHARED | PRIVATE combination
    MAP_SHARED = 0x01,
    MAP_PRIVATE = 0x03,
    MAP_FIXED = 0x04,
}

enum {
    MAPFD_ANON = -1,
}

struct MemMapping {
    void* base;
    size_t length;
    int refcnt;
    int flags;
    int prot;
    FileDescriptor* fd;
    off_t offset;
}

struct MemMappingPtr {
    void* base;
    MemMapping* mmap;
}

__gshared MemMapping[MAX_MMAP] mmaps;

void* sysMmap(
        Process* process,
        bool phy,
        void* addr,
        size_t length,
        int prot,
        int flags,
        int fd,
        off_t offset) {
    Process* tmp = cprocess;
    cprocess = process;
    void* ret = syscallMmap(addr, length, prot, flags, fd, offset);
    cprocess = tmp;
    if (cast(ssize_t)ret < 0) {
        return ret;
    }
    if (phy) {
        return getPhysicalMapping(process.cr3, cast(size_t)ret);
    } else {
        return ret;
    }
}

void* pageMmap(
        Process* process,
        size_t iaddr,
        size_t length,
        MemMapping* m,
        int prot,
        MemMappingPtr* mp) {
    void* addr = cast(void*)iaddr;
    if (m.base == null) {
        return cast(void*)-EINVAL;
    }
    for (size_t j = 0;; j += 4096) {
        void* p = createPage(
            cast(ulong*)process.cr3,
            iaddr,
            PAGE_UP | 1 | (prot & PROT_WRITE),
            m.base + j);
        if (p == null) {
            for (size_t k = cast(size_t)addr; k < iaddr; k += 4096) {
                unmapPage(cast(ulong*)process.cr3, k);
            }
            mp.mmap = null;
            return cast(void*)-ENOMEM;
        }
        if (length <= 4096) {
            break;
        }
        length -= 4096;
        iaddr += 4096;
    }
    return addr;
}

MemMapping* mmap(
        size_t length,
        int prot,
        int flags,
        FileDescriptor* fd,
        off_t offset) {
    for (size_t i = 0; i < MAX_MMAP; i++) {
        if (mmaps[i].refcnt == 0) {
            mmaps[i].length = length;
            if (fd == null) {
                if (length % 4096 != 0) {
                    length += 4096 - length % 4096;
                }
                mmaps[i].base = _calloc(length, 4096);
                if (mmaps[i].base == null) {
                    return null;
                }
            } else {
                mmaps[i].base = null;
            }
            mmaps[i].refcnt = 1;
            mmaps[i].flags = flags;
            mmaps[i].prot = prot;
            mmaps[i].fd = fd;
            mmaps[i].offset = offset;
            return &mmaps[i];
        }
    }
    return null;
}

int munmap(MemMapping* m, size_t length) {
    m.refcnt--;
    if (m.refcnt <= 0) {
        if (m.base != null) {
            _free(m.base);
        }
    }
    return 0;
}
