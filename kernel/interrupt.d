module kernel.interrupt;

import kernel.io;
import kernel.util;

@nogc:
nothrow:

struct idtr_t {
align(1):
    ushort limit;
    size_t base;
}

align(2) __gshared idtr_t idtr;

struct idtDescriptor_t {
align(1):
    ushort offsetLow;
    ushort selector;
    ubyte ist;
    ubyte typeAttr;
    ushort offsetMid;
    uint offsetHigh;
    uint zero;
}

align(16) __gshared idtDescriptor_t[256] idt;

enum GateType : ubyte {
    TaskGate32 = 0x5,
    IntGate16 = 0x6,
    TrapGate16 = 0x7,
    IntGate32 = 0xE,
    TrapGate32 = 0xF,
}

void interruptInit() {
    idtr.limit = idt.sizeof - 1;
    idtr.base = cast(size_t)&idt;
    size_t idtr_addr = cast(size_t)&idtr;
    asm @nogc nothrow {
        mov RAX, idtr_addr;
        lidt [RAX];
    }
}

struct Registers {
    ulong rax, rbx, rcx, rdx, rsi, rdi, rbp, rsp,
           r8,  r9, r10, r11, r12, r13, r14, r15;
    ulong rip, rflags;
    ushort cs, ss, ds, es, fs, gs;
    ulong errorCode;
}

template wrapInterrupt(string F, bool R=true, bool E=false) {
    enum string wrapInterrupt = `
        __gshared Registers ` ~ F ~ `_r;
        void ` ~ F ~ `_h() {
            asm @nogc nothrow {
                naked;
                push RBX;
                lea RBX, ` ~ F ~ `_r[RIP];
                mov Registers.rax.offsetof[RBX], RAX;
                pop RAX;
                mov Registers.rbx.offsetof[RBX], RAX;` ~ (E ? `
                pop RAX;
                mov Registers.errorCode.offsetof[RBX], RAX;` : "") ~
                `mov Registers.rcx.offsetof[RBX], RCX;
                mov Registers.rdx.offsetof[RBX], RDX;
                mov Registers.rdi.offsetof[RBX], RDI;
                mov Registers.rsi.offsetof[RBX], RSI;
                mov Registers.rbp.offsetof[RBX], RBP;
                mov RAX, 24[RSP];
                mov Registers.rsp.offsetof[RBX], RAX;
                mov Registers.r8.offsetof[RBX], R8;
                mov Registers.r9.offsetof[RBX], R9;
                mov Registers.r10.offsetof[RBX], R10;
                mov Registers.r11.offsetof[RBX], R11;
                mov Registers.r12.offsetof[RBX], R12;
                mov Registers.r13.offsetof[RBX], R13;
                mov Registers.r14.offsetof[RBX], R14;
                mov Registers.r15.offsetof[RBX], R15;
                mov RAX, [RSP];
                mov Registers.rip.offsetof[RBX], RAX;
                mov RAX, 16[RSP];
                mov Registers.rflags.offsetof[RBX], RAX;
                mov RAX, 8[RSP];
                mov Registers.cs.offsetof[RBX], RAX;
                mov RAX, 32[RSP];
                mov Registers.ss.offsetof[RBX], RAX;
                mov AX, DS;
                mov Registers.ds.offsetof[RBX], AX;
                mov AX, ES;
                mov Registers.es.offsetof[RBX], AX;
                mov AX, FS;
                mov Registers.fs.offsetof[RBX], AX;
                mov AX, GS;
                mov Registers.gs.offsetof[RBX], AX;
                mov AX, SEG_DS_KERNEL;
                mov SS, AX;
                mov DS, AX;
                mov ES, AX;
                mov FS, AX;
                mov GS, AX;
                cli;
                call ` ~ F ~ ";" ~ (R ? "" : "push RAX;") ~ `
                mov AX, Registers.ds.offsetof[RBX];
                mov DS, AX;
                mov AX, Registers.es.offsetof[RBX];
                mov ES, AX;
                mov AX, Registers.fs.offsetof[RBX];
                mov FS, AX;
                mov AX, Registers.gs.offsetof[RBX];
                mov GS, AX;` ~ (R ?
                `mov RAX, Registers.rax.offsetof[RBX];
                mov RCX, Registers.rcx.offsetof[RBX];
                mov RDX, Registers.rdx.offsetof[RBX];
                mov RDI, Registers.rdi.offsetof[RBX];
                mov RSI, Registers.rsi.offsetof[RBX];
                mov RBP, Registers.rbp.offsetof[RBX];
                mov R8, Registers.r8.offsetof[RBX];
                mov R9, Registers.r9.offsetof[RBX];
                mov R10, Registers.r10.offsetof[RBX];
                mov R11, Registers.r11.offsetof[RBX];
                mov R12, Registers.r12.offsetof[RBX];
                mov R13, Registers.r13.offsetof[RBX];
                mov R14, Registers.r14.offsetof[RBX];
                mov R15, Registers.r15.offsetof[RBX];
                mov RBX, Registers.rbx.offsetof[RBX];` : "pop RAX;") ~
                `iretq;
            }
        }
    `;
}

void interruptSetDescriptor(
    long i, size_t offset, ushort selector, ubyte ist,
    bool present, ubyte dpl, bool storageSegment, GateType type) {
    idt[i].offsetLow = cast(ushort)(offset & 0xFFFF);
    idt[i].offsetMid = cast(ushort)((offset >> 16) & 0xFFFF);
    idt[i].offsetHigh = cast(uint)(offset >> 32);
    idt[i].ist = ist;
    idt[i].selector = selector;
    idt[i].zero = 0;
    idt[i].typeAttr = cast(ubyte)(type
                                | (storageSegment << 4)
                                | (dpl << 5)
                                | (present << 7));
}

void interruptEnable() {
    asm @nogc nothrow {
        sti;
    }
}
void interruptDisable() {
    asm @nogc nothrow {
        cli;
    }
}

void irqEOI(long irq) {
    if (irq >= 8) {
        portOut(0xA0, 0x20);
    }
    portOut(0x20, 0x20);
}

void irqRemap(ubyte offset) {
    ubyte maskMaster = portIn(0x21);
    ubyte maskSlave = portIn(0xA1);

    portOut(0x20, 0x11);
    portOut(0xA0, 0x11);
    portOut(0x21, offset);
    portOut(0xA1, cast(ubyte)(offset + 8));
    portOut(0x21, 4);
    portOut(0xA1, 2);
    portOut(0x21, 0x01);
    portOut(0xA1, 0x01);

    portOut(0x21, maskMaster);
    portOut(0xA1, maskSlave);
}

ushort irqMask() {
    return cast(ushort)(portIn(0x21) | (portIn(0xA1) << 8));
}
void irqMask(ushort mask) {
    portOut(0x21, cast(ubyte)(mask & 0xFF));
    portOut(0xA1, cast(ubyte)(mask >> 8));
}

ushort irqIRR() {
    portOut(0x20, 0x0A);
    portOut(0xA0, 0x0A);
    return cast(ushort)(portIn(0x20) | (portIn(0xA0) << 8));
}
ushort irqISR() {
    portOut(0x20, 0x0B);
    portOut(0xA0, 0x0B);
    return cast(ushort)(portIn(0x20) | (portIn(0xA0) << 8));
}

void pitSetDivider(ushort divider) {
    portOut(0x43, 0b00110100);
    portOut(0x40, cast(ubyte)(divider & 0xFF));
    portOut(0x40, cast(ubyte)(divider >> 8));
}

struct tss_t {
align(1):
    uint _0;
    ulong RSP0;
    ulong RSP1;
    ulong RSP2;
    ulong _1;
    ulong IST1;
    ulong IST2;
    ulong IST3;
    ulong IST4;
    ulong IST5;
    ulong IST6;
    ulong IST7;
    ulong _2;
    ushort _3;
    ushort IOPB;
}

align(8) __gshared tss_t tss;

extern(C) void asm_loadTSS();
extern(C) extern __gshared ulong stack_end;

void tssInit(size_t gdt) {
    ulong *gdt_ptr = cast(ulong*)(gdt + SEG_TSS);
    size_t tss_addr = cast(size_t)&tss;
    gdt_ptr[0] |= ((tss_addr & 0xFFFF) << 16)
                | (((tss_addr >> 16) & 0xFF) << 32)
                | (((tss_addr >> 24) & 0xFF) << 56);
    gdt_ptr[1] = tss_addr >> 32;
    tss.IOPB = tss_t.sizeof;
    tss.RSP0 = cast(ulong)&stack_end;
    asm_loadTSS;
}
