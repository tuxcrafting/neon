module kernel.memory;

import kernel.config;
import kernel.util;

@nogc:
nothrow:

struct MemoryRange {
    size_t base;
    size_t limit;
    size_t indexBase;
    size_t indexLimit;
}

__gshared MemoryRange[64] memoryRanges;
__gshared size_t totalAvailable;
__gshared ubyte* allocBitmap;
__gshared size_t lastAllocated;

void* bitmapTranslate(size_t i, size_t s, size_t a, ubyte* e) {
    for (long j = 0; memoryRanges[j].limit != 0; j++) {
        if (i >= memoryRanges[j].indexBase && i < memoryRanges[j].indexLimit) {
            if (i + s >= memoryRanges[j].indexLimit) {
                return null;
            }
            size_t addr = memoryRanges[j].base
                        + (i - memoryRanges[j].indexBase) * BLOCK_SIZE;
            if ((addr & (a - 1)) != 0) {
                if (e != null) {
                    *e = 1;
                }
                return null;
            }
            return cast(void*)addr;
        }
    }
    return null;
}

size_t bitmapTranslate(void* a) {
    size_t addr = explicitCast!(void*, size_t)(a);
    for (long i = 0; memoryRanges[i].limit != 0; i++) {
        if (addr >= memoryRanges[i].base && addr < memoryRanges[i].limit) {
            return (addr - memoryRanges[i].base) / BLOCK_SIZE
                 + memoryRanges[i].indexBase;
        }
    }
    return 0;
}

void* malloc(size_t size, size_t alignement=1) {
    void* ptr = _malloc(size, alignement);
    if (ptr == null) {
        return null;
    }
    return getVMA(ptr);
}

void* _malloc(size_t size, size_t alignement=1) {
    if (size % BLOCK_SIZE != 0) {
        size += BLOCK_SIZE;
    }
    size /= BLOCK_SIZE;
    if (size == 0) {
        return null;
    }
    size_t base = lastAllocated;
    size_t run = 0;
    for (size_t i = base, j = 0; j < totalAvailable / BLOCK_SIZE; i++, j++) {
        if (i == totalAvailable / BLOCK_SIZE) {
            i = 0;
        }
        ubyte n = allocBitmap[i / 4];
        if (((n >> (i % 4 * 2)) & 1) == 0) {
            run += 1;
        } else {
            base = i + 1;
            run = 0;
        }
        if (run == size) {
            ubyte e = 0;
            void* p = bitmapTranslate(base, size, alignement, &e);
            if (p == null) {
                if (e == 1) {
                    i += 1;
                    base = i;
                    run = 0;
                }
                continue;
            }
            for (size_t k = base; k < base + size; k++) {
                allocBitmap[k / 4] |= 1 << (k % 4 * 2);
                if (i == base + size - 1) {
                    allocBitmap[k / 4] &= ~(1 << (k % 4 * 2 + 1));
                } else {
                    allocBitmap[k / 4] |= 1 << (k % 4 * 2 + 1);
                }
            }
            lastAllocated = base + size;
            return p;
        }
    }
    return null;
}

void* calloc(size_t size, size_t alignement=1) {
    void* ptr = _calloc(size, alignement);
    if (ptr == null) {
        return null;
    }
    return getVMA(ptr);
}

void* _calloc(size_t size, size_t alignement=1) {
    void* ptr = _malloc(size, alignement);
    if (ptr == null) {
        return null;
    }
    memset(ptr + KERNEL_VMA_BASE, 0, size);
    return ptr;
}

void free(void* ptr) {
    _free(ptr - KERNEL_VMA_BASE);
}

void _free(void* ptr) {
    size_t index = bitmapTranslate(ptr);
    if (index == 0) {
        return;
    }
    for (size_t i = index;; i++) {
        if ((allocBitmap[i / 4] & (1 << (i % 4 * 2))) == 0) {
            return;
        }
        allocBitmap[i / 4] &= ~(1 << (i % 4 * 2));
        if ((allocBitmap[i / 4] & (1 << (i % 4 * 2 + 1))) == 0) {
            return;
        }
        allocBitmap[i / 4] &= ~(1 << (i % 4 * 2 + 1));
    }
    lastAllocated = index / BLOCK_SIZE;
}

extern(C) void memset(void* s, int c, size_t n) {
    for (size_t i = 0; i < n; i++) {
        (cast(ubyte*)s)[i] = cast(ubyte)c;
    }
}
extern(C) void* memcpy(void* dest, void* src, size_t n) {
    for (size_t i = 0; i < n; i++) {
        (cast(ubyte*)dest)[i] = (cast(ubyte*)src)[i];
    }
    return dest;
}
extern(C) void memcpy_r(void* dest, void* src, size_t n) {
    while (n--) {
        (cast(ubyte*)dest)[n] = (cast(ubyte*)src)[n];
    }
}
extern(C) void memmove(void* dest, void* src, size_t n) {
    if (src > dest) {
        memcpy(dest, src, n);
    } else {
        memcpy_r(dest, src, n);
    }
}
extern(C) int memcmp(void* a, void* b, size_t n) {
    for (size_t i = 0; i < n; i++) {
        if ((cast(ubyte*)a)[i] != (cast(ubyte*)b)[i]) {
            return 1;
        }
    }
    return 0;
}

extern(C) size_t strlen(char* s) {
    size_t l = 0;
    while (*s++) {
        l++;
    }
    return l;
}
