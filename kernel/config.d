module kernel.config;

import kernel.types;

enum pid_t MAX_PID = 2048;
enum int MAX_FD = 16384;
enum int MAX_FD_PER_PID = 512;
enum int MAX_MOUNT_POINTS = 32;
enum size_t PIT_FREQUENCY = 50;
enum size_t MAX_PATH_LENGTH = 256;
enum int MAX_LINKS_FOLLOWED = 32;
enum size_t BLOCK_SIZE = 128;
enum size_t MAX_MMAP = 65536;
enum size_t MAX_MMAP_PER_PID = 512;
