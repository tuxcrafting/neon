module kernel.stat;

import kernel.types;

@nogc:
nothrow:

struct stat_t {
    dev_t st_dev;
    ino_t st_ino;
    mode_t st_mode;
    nlink_t st_nlink;
    uid_t st_uid;
    gid_t st_gid;
    dev_t st_rdev;
    off_t st_size;
    timespec_t st_atim;
    timespec_t st_mtim;
    timespec_t st_ctim;
    blksize_t st_blksize;
    blkcnt_t st_blocks;
}

enum {
    S_IRWXU = 0b111_000_000,
    S_IRUSR = 0b100_000_000,
    S_IWUSR = 0b010_000_000,
    S_IXUSR = 0b001_000_000,

    S_IRWXG = 0b000_111_000,
    S_IRGRP = 0b000_100_000,
    S_IWGRP = 0b000_010_000,
    S_IXGRP = 0b000_001_000,

    S_IRWXO = 0b000_000_111,
    S_IROTH = 0b000_000_100,
    S_IWOTH = 0b000_000_010,
    S_IXOTH = 0b000_000_001,

    S_ISVTX = 0b001_000000000,
    S_ISGID = 0b010_000000000,
    S_ISUID = 0b100_000000000,

    S_IFMT = 0b1111_000000000000,
    S_IFIFO = 0b0001_000000000000,
    S_IFCHR = 0b0010_000000000000,
    S_IFDIR = 0b0100_000000000000,
    S_IFBLK = 0b0110_000000000000,
    S_IFREG = 0b1000_000000000000,
    S_IFLNK = 0b1010_000000000000,
    S_IFSOCK = 0b1100_000000000000,
}
