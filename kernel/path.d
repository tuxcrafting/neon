module kernel.path;

import kernel.errno;
import kernel.config;
import kernel.fcntl;
import kernel.fd;
import kernel.memory;
import kernel.process;
import kernel.types;

@nogc:
nothrow:

size_t basenameSplit(kstring path) {
    size_t slashI = 0;
    foreach (size_t i, char c; path) {
        if (c == '/') {
            slashI = i + 1;
        }
    }
    return slashI;
}

void nextComponent(kstring path, size_t* start, size_t* length) {
    *start += *length;
    *length = 0;
    size_t i;
    for (i = *start; i < path.length; i++) {
        (*length)++;
        if (path[i] == '/') {
            return;
        }
    }
    if (i == path.length) {
        (*length)++;
    }
}

int getAt(
        Process* process,
        int dirfd,
        char* buf,
        size_t* buflen,
        kstring path) {
    if (path[0] == '/') {
        *buflen = path.length;
        memcpy(buf, path.ptr, path.length);
    } else {
        kstring base;
        if (dirfd == AT_FDCWD) {
            base = mkstring(
                cast(char*)process.cwd.ptr,
                process.cwdLength);
        } else {
            if (dirfd < 0 || dirfd >= MAX_FD_PER_PID) {
                return -EBADF;
            }
            FileDescriptor* dfd = process.fds[dirfd];
            if (dfd == null) {
                return -EBADF;
            } else {
                if (dfd.type != FDType.Dir) {
                    return -ENOTDIR;
                }
                base = mkstring(
                    cast(char*)dfd.path.ptr,
                    dfd.path.length + 1);
                base.ptr[dfd.path.length] = '/';
            }
        }
        if (base.length + path.length > MAX_PATH_LENGTH) {
            return -ENAMETOOLONG;
        }
        memcpy(buf, base.ptr, base.length);
        memcpy(buf + base.length, path.ptr, path.length);
        *buflen = base.length + path.length;
    }
    return 0;
}
