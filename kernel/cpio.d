module kernel.cpio;

import kernel.memory;
import kernel.types;
import kernel.util;

@nogc:
nothrow:

struct CpioOdcHeader {
align(1):
    char[6] cMagic;
    char[6] cDev;
    char[6] cIno;
    char[6] cMode;
    char[6] cUid;
    char[6] cGid;
    char[6] cNlink;
    char[6] cRdev;
    char[11] cMtime;
    char[6] cNamesize;
    char[11] cFilesize;
}

struct CpioOdcEntry {
    int dev;
    int ino;
    int mode;
    int uid;
    int gid;
    int nlink;
    int rdev;
    long mtime;
    size_t nameSize;
    size_t fileSize;
    char* name;
    ubyte* file;
    ubyte* next;
    ubyte* base;
}

int cpioParseEntry(CpioOdcEntry* entry, ubyte* data) {
    CpioOdcHeader* header = cast(CpioOdcHeader*)data;
    if (parseOctal(header.cMagic.ptr, 6) != 29127) {
        return 1;
    }
    entry.dev = cast(int)parseOctal(header.cDev.ptr, 6);
    entry.ino = cast(int)parseOctal(header.cIno.ptr, 6);
    entry.mode = cast(int)parseOctal(header.cMode.ptr, 6);
    entry.uid = cast(int)parseOctal(header.cUid.ptr, 6);
    entry.gid = cast(int)parseOctal(header.cGid.ptr, 6);
    entry.nlink = cast(int)parseOctal(header.cNlink.ptr, 6);
    entry.rdev = cast(int)parseOctal(header.cRdev.ptr, 6);
    entry.mtime = cast(long)parseOctal(header.cMtime.ptr, 11);
    entry.nameSize = cast(long)parseOctal(header.cNamesize.ptr, 6) - 1;
    entry.fileSize = cast(long)parseOctal(header.cFilesize.ptr, 11);
    entry.name = cast(char*)(data + CpioOdcHeader.sizeof);
    entry.file = cast(ubyte*)(data + CpioOdcHeader.sizeof
                            + entry.nameSize + 1);
    entry.next = data + CpioOdcHeader.sizeof
                      + entry.nameSize + 1
                      + entry.fileSize;
    entry.base = data;
    return 0;
}

__gshared kstring cpioTrailer = KS!"TRAILER!!!";

bool cpioFindFile(CpioOdcEntry* entry, ubyte* data, kstring path) {
    entry.next = data;
    for (;;) {
        cpioParseEntry(entry, entry.next);
        if (entry.nameSize == 10
         && memcmp(
                cast(void*)entry.name,
                cast(void*)cpioTrailer.ptr, 10) == 0) {
            return false;
        }
        if (entry.nameSize == path.length
         && memcmp(
                cast(void*)entry.name,
                cast(void*)path.ptr, path.length) == 0) {
            break;
        }
    }
    return true;
}
