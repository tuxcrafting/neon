module kernel.signal;

import kernel.types;

enum {
    SIGHUP = 1,
    SIGINT = 2,
    SIGQUIT = 3,
    SIGILL = 4,
    SIGTRAP = 5,
    SIGABRT = 6,
    SIGIOT = 6,
    SIGBUS = 7,
    SIGFPE = 8,
    SIGKILL = 9,
    SIGUSR1 = 10,
    SIGSEGV = 11,
    SIGUSR2 = 12,
    SIGPIPE = 13,
    SIGALRM = 14,
    SIGTERM = 15,
    SIGCHLD = 16,
    SIGCONT = 17,
    SIGSTOP = 18,
    SIGTSTP = 19,
    SIGTTIN = 20,
    SIGTTOU = 21,
    SIGURG = 22,
    SIGXCPU = 23,
    SIGXFSZ = 24,
    SIGVTALRM = 25,
    SIGPROF = 26,
    SIGPOLL = 27,
}

enum {
    SA_NOCLDSTOP = 0x0001,
    SA_ONSTACK = 0x0002,
    SA_RESETHAND = 0x0004,
    SA_RESTART = 0x0008,
    SA_SIGINFO = 0x0010,
    SA_NOCLDWAIT = 0x0020,
    SA_NODEFER = 0x0040,
    SA_RESTORER = 0x8000,
}

enum {
    SIG_DFL = 0,
    SIG_ERR = -1,
    SIG_HOLD = -2,
    SIG_IGN = -3,
}

enum {
    __SIG_ACTION_INV,
    __SIG_ACTION_T,
    __SIG_ACTION_A,
    __SIG_ACTION_I,
    __SIG_ACTION_S,
    __SIG_ACTION_C,
}

__gshared int[] signalDefaultActions = [
    __SIG_ACTION_INV,
    __SIG_ACTION_T, // SIGHUP
    __SIG_ACTION_T, // SIGINT
    __SIG_ACTION_T, // SIGQUIT
    __SIG_ACTION_A, // SIGILL
    __SIG_ACTION_A, // SIGTRAP
    __SIG_ACTION_A, // SIGABRT
    __SIG_ACTION_A, // SIGBUS
    __SIG_ACTION_A, // SIGFPE
    __SIG_ACTION_T, // SIGKILL
    __SIG_ACTION_T, // SIGUSR1
    __SIG_ACTION_A, // SIGSEGV
    __SIG_ACTION_T, // SIGUSR2
    __SIG_ACTION_T, // SIGPIPE
    __SIG_ACTION_T, // SIGALRM
    __SIG_ACTION_T, // SIGTERM
    __SIG_ACTION_I, // SIGCHLD
    __SIG_ACTION_C, // SIGCONT
    __SIG_ACTION_S, // SIGSTOP
    __SIG_ACTION_S, // SIGTSTP
    __SIG_ACTION_S, // SIGTTIN
    __SIG_ACTION_S, // SIGTTOU
    __SIG_ACTION_I, // SIGURG
    __SIG_ACTION_A, // SIGXCPU
    __SIG_ACTION_A, // SIGXFSZ
    __SIG_ACTION_T, // SIGVTALRM
    __SIG_ACTION_T, // SIGPROF
    __SIG_ACTION_T, // SIGPOLL
];

struct sigaction_t {
    union {
        void function(int) sa_handler;
        void function(int, siginfo_t*, void*) sa_sigaction;
        int _sa_int;
    }
    sigset_t sa_mask;
    int sa_flags;
    void* sa_restorer;
}

enum {
    ILL_ILLOPC = 0x01,
    ILL_ILLOPN = 0x02,
    ILL_ILLADR = 0x03,
    ILL_ILLTRP = 0x04,
    ILL_PRVOPC = 0x05,
    ILL_PRVREG = 0x06,
    ILL_COPROC = 0x07,
    ILL_BADSTK = 0x08,
    FPE_INTDIV = 0x11,
    FPE_INTOVF = 0x12,
    FPE_FLTDIV = 0x13,
    FPE_FLTOVF = 0x14,
    FPE_FLTUND = 0x15,
    FPE_FLTRES = 0x16,
    FPE_FLTINV = 0x17,
    FPE_FLTSUB = 0x18,
    SEGV_MAPERR = 0x21,
    SEGV_ACCERR = 0x22,
    BUS_ADRALN = 0x31,
    BUS_ADRERR = 0x32,
    BUS_OBJERR = 0x33,
    TRAP_BRKPT = 0x41,
    TRAP_TRACE = 0x42,
    CLD_EXITED = 0x51,
    CLD_KILLED = 0x52,
    CLD_DUMPED = 0x53,
    CLD_TRAPPED = 0x54,
    CLD_STOPPED = 0x55,
    CLD_CONTINUED = 0x56,
    POLL_IN = 0x61,
    POLL_OUT = 0x62,
    POLL_MSG = 0x63,
    POLL_ERR = 0x64,
    POLL_PRI = 0x65,
    POLL_HUP = 0x66,
    SI_USER = 0xF1,
    SI_QUEUE = 0xF2,
    SI_TIMER = 0xF3,
    SI_ASYNCIO = 0xF4,
    SI_MESGQ = 0xF5,
}

struct stack_t {
    void* ss_sp;
    size_t ss_size;
    int ss_flags;
}

union sigval_t {
    int sival_int;
    void* sival_ptr;
}

struct mcontext_t {
    ulong rax, rbx, rcx, rdx, rsi, rdi, rbp, rsp,
           r8,  r9, r10, r11, r12, r13, r14, r15;
    ulong rip, rflags;
}

struct ucontext_t {
    ucontext_t* uc_link;
    sigset_t uc_sigmask;
    stack_t uc_stack;
    mcontext_t uc_mcontext;
}

struct siginfo_t {
    int si_signo;
    int si_code;
    int si_errno;
    pid_t si_pid;
    uid_t si_uid;
    void* si_addr;
    int si_status;
    long si_band;
    sigval_t si_value;
}
