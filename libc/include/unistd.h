#pragma once

#include <stdnoreturn.h>
#include <sys/types.h>

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

extern char **environ;

noreturn void _exit(int status);

int close(int fd);

ssize_t write(int fd, const void *buf, size_t n);
ssize_t pwrite(int fd, const void *buf, size_t n, off_t offset);

ssize_t read(int fd, void *buf, size_t n);
ssize_t pread(int fd, void *buf, size_t n, off_t offset);

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END -1

off_t lseek(int fd, off_t offset, int whence);

int fork();

int fexecve(int fd, char *const argv[], char *const envp[]);
int execve(const char *filename, char *const argv[], char *const envp[]);
