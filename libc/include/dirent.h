#pragma once

#include <limits.h>
#include <sys/types.h>

typedef struct {
    int fd;
} DIR;

struct dirent {
    ino_t d_ino;
    char d_name[NAME_MAX];
};

DIR *opendir(const char *path);
DIR *fdopendir(int fd);

int dirfd(DIR *dir);
int closedir(DIR *dir);

struct dirent *readdir(DIR *dir);
