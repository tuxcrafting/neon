#pragma once

#include <stddef.h>

void *memcpy(void *dest, const void *src, size_t n);
void *memmove(void *dest, const void *src, size_t n);
void *memset(void *dest, int c, size_t n);

size_t strlen(const char *s);
size_t strnlen(const char *s, size_t maxlen);

char *strcpy(char *dest, const char *src);

int strcmp(const char *a, const char *b);

char *strtok_r(char *str, const char *delim, char **saveptr);
char *strtok(char *str, const char *delim);

char *strerror(int errnum);
