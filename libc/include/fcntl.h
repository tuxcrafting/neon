#pragma once

#include <stdio.h>
#include <unistd.h>

#define F_DUPFD 0
#define F_DUPFD_CLOEXEC 1
#define F_GETFD 2
#define F_SETFD 3
#define F_GETFL 4
#define F_SETFL 5
#define F_GETLK 6
#define F_SETLK 7
#define F_SETLKW 8
#define F_GETOWN 9
#define F_SETOWN 10

#define F_RDLCK 0
#define F_UNLCK 1
#define F_WRLCK 2

#define O_RDONLY 0x00000001
#define O_WRONLY 0x00000002
#define O_RDWR 0x00000003
#define O_ACCMODE 0x00000003

#define O_CLOEXEC 0x00000004
#define O_CREAT 0x00000008
#define O_DIRECTORY 0x00000010
#define O_EXCL 0x00000020
#define O_NOCTTY 0x00000040
#define O_NOFOLLOW 0x00000080
#define O_TRUNC 0x00000100
#define O_TTY_INIT 0x00000200
#define O_APPEND 0x00000400
#define O_SYNC 0x00000800
#define O_NONBLOCK 0x00001000

#define O_NOFOLLOW_SUCCESS 0x00100000

#define AT_FDCWD -1

#define AT_SYMLINK_NOFOLLOW 1
#define AT_SYMLINK_FOLLOW 1
#define AT_REMOVEDIR 1

#define POSIX_FADV_DONTNEED 0
#define POSIX_FADV_NOREUSE 1
#define POSIX_FADV_NORMAL 2
#define POSIX_FADV_RANDOM 3
#define POSIX_FADV_SEQUENTIAL 4
#define POSIX_FADV_WILLNEED 5

int openat(int fd, const char *path, int oflag);
int open(const char *path, int oflag);
