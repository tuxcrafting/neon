#pragma once

#include <stddef.h>

typedef intptr_t ssize_t;
typedef ssize_t blkcnt_t;
typedef size_t blksize_t;
typedef size_t clock_t;
typedef size_t clockid_t;
typedef size_t dev_t;
typedef size_t fsblkcnt_t;
typedef size_t fsfilcnt_t;
typedef int gid_t;
typedef int id_t;
typedef size_t ino_t;
typedef int key_t;
typedef int mode_t;
typedef ssize_t nlink_t;
typedef ssize_t off_t;
typedef int pid_t;
typedef ssize_t suseconds_t;
typedef ssize_t time_t;
typedef int timer_t;
typedef int uid_t;

#define SSIZE_MAX INTPTR_MAX
