#pragma once

#include <sys/types.h>

#define PROT_NONE 0
#define PROT_READ 1
#define PROT_WRITE 2
#define PROT_EXEC 4

#define MAP_SHARED 0x01
#define MAP_PRIVATE 0x03
#define MAP_FIXED 0x04

#define MAP_ANONYMOUS 0x00010000

#define MAPFD_ANON -1

void *mmap(
        void *addr,
        size_t length,
        int prot,
        int flags,
        int fd,
        off_t offset);
int munmap(void *addr, size_t length);
