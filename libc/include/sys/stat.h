#pragma once

#include <time.h>
#include <sys/types.h>

#define S_IRWXU 0b111000000
#define S_IRUSR 0b100000000
#define S_IWUSR 0b010000000
#define S_IXUSR 0b001000000

#define S_IRWXG 0b000111000
#define S_IRGRP 0b000100000
#define S_IWGRP 0b000010000
#define S_IXGRP 0b000001000

#define S_IRWXO 0b000000111
#define S_IROTH 0b000000100
#define S_IWOTH 0b000000010
#define S_IXOTH 0b000000001

#define S_ISVTX 0b001000000000
#define S_ISGID 0b010000000000
#define S_ISUID 0b100000000000

#define S_IFMT 0b1111000000000000
#define S_IFIFO 0b0001000000000000
#define S_IFCHR 0b0010000000000000
#define S_IFDIR 0b0100000000000000
#define S_IFBLK 0b0110000000000000
#define S_IFREG 0b1000000000000000
#define S_IFLNK 0b1010000000000000
#define S_IFSOCK 0b1100000000000000

#define S_ISBLK(m) (((m) & S_IFMT) == S_IFBLK)
#define S_ISCHR(m) (((m) & S_IFMT) == S_IFCHR)
#define S_ISDIR(m) (((m) & S_IFMT) == S_IFDIR)
#define S_ISFIFO(m) (((m) & S_IFMT) == S_IFIFO)
#define S_ISREG(m) (((m) & S_IFMT) == S_IFREG)
#define S_ISLNK(m) (((m) & S_IFMT) == S_IFLNK)
#define S_ISSOCK(m) (((m) & S_IFMT) == S_IFSOCK)

struct stat {
    dev_t st_dev;
    ino_t st_ino;
    mode_t st_mode;
    nlink_t st_nlink;
    uid_t st_uid;
    gid_t st_gid;
    dev_t st_rdev;
    off_t st_size;
    struct timespec st_atim;
    struct timespec st_mtim;
    struct timespec st_ctim;
    blksize_t st_blksize;
    blkcnt_t st_blocks;
};

int fstat(int fd, struct stat *buf);
int fstatat(int fd, const char *path, struct stat *buf, int flag);
int stat(const char *path, struct stat *buf);
int lstat(const char *path, struct stat *buf);
