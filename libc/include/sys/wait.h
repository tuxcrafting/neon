#pragma once

#include <signal.h>

#define P_PID 0
#define P_PGID 1
#define P_ALL 2

#define WCONTINUED 0x01
#define WEXITED 0x02
#define WNOHANG 0x04
#define WNOWAIT 0x08
#define WSTOPPED 0x10

typedef id_t idtype_t;

int waitid(idtype_t idtype, id_t id, siginfo_t *infop, int options);
