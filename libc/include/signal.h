#pragma once

#include <sys/types.h>

#define SIGHUP 1
#define SIGINT 2
#define SIGQUIT 3
#define SIGILL 4
#define SIGTRAP 5
#define SIGABRT 6
#define SIGIOT 6
#define SIGBUS 7
#define SIGFPE 8
#define SIGKILL 9
#define SIGUSR1 10
#define SIGSEGV 11
#define SIGUSR2 12
#define SIGPIPE 13
#define SIGALRM 14
#define SIGTERM 15
#define SIGCHLD 16
#define SIGCONT 17
#define SIGSTOP 18
#define SIGTSTP 19
#define SIGTTIN 20
#define SIGTTOU 21
#define SIGURG 22
#define SIGXCPU 23
#define SIGXFSZ 24
#define SIGVTALRM 25
#define SIGPROF 26
#define SIGPOLL 27

#define SA_NOCLDSTOP 0x0001
#define SA_ONSTACK 0x0002
#define SA_RESETHAND 0x0004
#define SA_RESTART 0x0008
#define SA_SIGINFO 0x0010
#define SA_NOCLDWAIT 0x0020
#define SA_NODEFER 0x0040
#define SA_RESTORER 0x8000

#define SIG_DFL 0
#define SIG_ERR -1
#define SIG_HOLD -2
#define SIG_IGN -3

#define ILL_ILLOPC 0x01
#define ILL_ILLOPN 0x02
#define ILL_ILLADR 0x03
#define ILL_ILLTRP 0x04
#define ILL_PRVOPC 0x05
#define ILL_PRVREG 0x06
#define ILL_COPROC 0x07
#define ILL_BADSTK 0x08
#define FPE_INTDIV 0x11
#define FPE_INTOVF 0x12
#define FPE_FLTDIV 0x13
#define FPE_FLTOVF 0x14
#define FPE_FLTUND 0x15
#define FPE_FLTRES 0x16
#define FPE_FLTINV 0x17
#define FPE_FLTSUB 0x18
#define SEGV_MAPERR 0x21
#define SEGV_ACCERR 0x22
#define BUS_ADRALN 0x31
#define BUS_ADRERR 0x32
#define BUS_OBJERR 0x33
#define TRAP_BRKPT 0x41
#define TRAP_TRACE 0x42
#define CLD_EXITED 0x51
#define CLD_KILLED 0x52
#define CLD_DUMPED 0x53
#define CLD_TRAPPED 0x54
#define CLD_STOPPED 0x55
#define CLD_CONTINUED 0x56
#define POLL_IN 0x61
#define POLL_OUT 0x62
#define POLL_MSG 0x63
#define POLL_ERR 0x64
#define POLL_PRI 0x65
#define POLL_HUP 0x66
#define SI_USER 0xF1
#define SI_QUEUE 0xF2
#define SI_TIMER 0xF3
#define SI_ASYNCIO 0xF4
#define SI_MESGQ 0xF5

union sigval {
    int sival_int;
    void *sival_ptr;
};

typedef struct {
    int si_signo;
    int si_code;
    int si_errno;
    pid_t si_pid;
    uid_t si_uid;
    void *si_addr;
    int si_status;
    long int si_band;
    union sigval si_value;
} siginfo_t;
