#pragma once

#include <stdint.h>

typedef intptr_t ptrdiff_t;
typedef uint16_t wchar_t;
typedef uintptr_t size_t;

#define NULL 0
