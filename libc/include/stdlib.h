#pragma once

#include <stdnoreturn.h>
#include <unistd.h>

#define _Exit _exit

noreturn void exit(int status);

void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
#define reallocarray(ptr, nmemb, size) realloc(ptr, (nmemb) * (size))
