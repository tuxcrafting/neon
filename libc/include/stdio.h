#pragma once

#include <sys/types.h>
#include <unistd.h>

#define __FILE_state_OK 0
#define __FILE_state_EOF 1
#define __FILE_state_ERROR 2

typedef struct FILE {
    int fd;
    int state;
} FILE;

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

FILE *fopen(const char *path, const char *mode);
FILE *fdopen(int fd, const char *mode);
int fclose(FILE *stream);

int fputc(int c, FILE *stream);
int putc(int c, FILE *stream);
int putchar(int c);

int fputs(const char *s, FILE *stream);
int puts(const char *s);

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);

#define clearerr(stream) ((stream)->state = __FILE_state_OK)
#define feof(stream) ((stream)->state == __FILE_state_EOF)
#define ferror(stream) ((stream)->state == __FILE_state_ERROR)
#define fileno(stream) ((stream)->fd)

typedef off_t fpos_t;

int fseeko(FILE *stream, off_t offset, int whence);
off_t ftello(FILE *stream);
int fseek(FILE *stream, long offset, int whence);
long ftell(FILE *stream);
void rewind(FILE *stream);
int fgetpos(FILE *stream, fpos_t *pos);
int fsetpos(FILE *stream, const fpos_t *pos);
