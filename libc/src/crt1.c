#include <stdlib.h>

extern int main(int argc, char *argv[], char *environ[]);

int errno;

char **environ;

__attribute__((naked)) void _start() {
    __asm__(
        "mov 0x0(%%rsp), %%rdi;" // arg[argc] = *(%rsp + 0x0)
        "lea 0x8(%%rsp), %%rsi;" // arg[argv] = %rsp + 0x8
        "mov %%rdi, %%rdx;"
        "shl $0x3, %%rdx;"
        "add %%rsi, %%rdx;"
        "add $0x8, %%rdx;" // arg[environ] = argv + argc * 8 + 8
        "mov %%rdx, %0;" // environ = arg[environ]
        "call main;"
        "mov %%rax, %%rdi;" // arg[status] = main()
        "call exit;"
        : "=m"(environ));
}

long int __errno_wrapper(long int n) {
    if (n < 0) {
        errno = -n;
        return -1;
    } else {
        return n;
    }
}
