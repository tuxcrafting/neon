#include <errno.h>
#include <string.h>

void *memcpy(void *dest, const void *src, size_t n) {
    for (size_t i = 0; i < n; i++) {
        *((uint8_t*)dest + i) = *((uint8_t*)src + i);
    }
    return dest;
}

void *memmove(void *dest, const void *src, size_t n) {
    if (dest > src) {
        while (n--) {
            *((uint8_t*)dest + n) = *((uint8_t*)src + n);
        }
    } else {
        for (size_t i = 0; i < n; i++) {
            *((uint8_t*)dest + i) = *((uint8_t*)src + i);
        }
    }
    return dest;
}

void *memset(void *dest, int c, size_t n) {
    for (size_t i = 0; i < n; i++) {
        *((uint8_t*)dest + i) = c;
    }
    return dest;
}

size_t strlen(const char *s) {
    size_t n = 0;
    while (*s++) {
        n++;
    }
    return n;
}

size_t strnlen(const char *s, size_t maxlen) {
    size_t n = 0;
    while (*s++ && maxlen--) {
        n++;
    }
    return n;
}

char *strcpy(char *dest, const char *src) {
    return memcpy(dest, src, strlen(src));
}

int strcmp(const char *a, const char *b) {
    int al = strlen(a);
    int bl = strlen(b);
    if (al != bl) {
        return al - bl;
    }
    for (int i = 0; i < al; i++) {
        if (a[i] != b[i]) {
            return 1;
        }
    }
    return 0;
}

char *strtok_r(char *str, const char *delim, char **saveptr) {
    if (str == NULL) {
        str = *saveptr;
    }
    char *ptr = str;
    int has_delim = 1;
    while (has_delim && *ptr) {
        const char *d = delim;
        has_delim = 0;
        while (*d) {
            if (*ptr == *d++) {
                has_delim = 1;
                break;
            }
        }
        if (has_delim) {
            ptr++;
        }
    }
    if (*ptr == '\0') {
        return NULL;
    }
    str = ptr;
    while (!has_delim && *ptr) {
        const char *d = delim;
        has_delim = 0;
        while (*d) {
            if (*ptr == *d++) {
                has_delim = 1;
                break;
            }
        }
        if (!has_delim) {
            ptr++;
        }
    }
    if (str == ptr) {
        return NULL;
    }
    if (*ptr == '\0') {
        *saveptr = ptr;
    } else {
        *ptr = '\0';
        *saveptr = ptr + 1;
    }
    return str;
}

char *strtok(char *str, const char *delim) {
    static char *saveptr;
    return strtok_r(str, delim, &saveptr);
}

char *strerror(int errnum) {
    static char strerrbuf[64];
    switch (errnum) {
    case E2BIG:
        strcpy(strerrbuf, "Argument list too long");
        break;
    case EACCES:
        strcpy(strerrbuf, "Permission denied");
        break;
    case EADDRINUSE:
        strcpy(strerrbuf, "Address in use");
        break;
    case EADDRNOTAVAIL:
        strcpy(strerrbuf, "Address not available");
        break;
    case EAFNOSUPPORT:
        strcpy(strerrbuf, "Address family not supported");
        break;
    case EAGAIN:
        strcpy(strerrbuf, "Resource unavailable, try again");
        break;
    case EALREADY:
        strcpy(strerrbuf, "Connection already in progress");
        break;
    case EBADF:
        strcpy(strerrbuf, "Bad file descriptor");
        break;
    case EBADMSG:
        strcpy(strerrbuf, "Bad message");
        break;
    case EBUSY:
        strcpy(strerrbuf, "Device or resource busy");
        break;
    case ECANCELED:
        strcpy(strerrbuf, "Operation canceled");
        break;
    case ECHILD:
        strcpy(strerrbuf, "No child processes");
        break;
    case ECONNABORTED:
        strcpy(strerrbuf, "Connection aborted");
        break;
    case ECONNREFUSED:
        strcpy(strerrbuf, "Connection refused");
        break;
    case ECONNRESET:
        strcpy(strerrbuf, "Connection reset");
        break;
    case EDEADLK:
        strcpy(strerrbuf, "Resource deadlock would occur");
        break;
    case EDESTADDRREQ:
        strcpy(strerrbuf, "Destination address required");
        break;
    case EDOM:
        strcpy(strerrbuf, "Mathematics argument out of domain of function");
        break;
    case EDQUOT:
        strcpy(strerrbuf, "Reserved");
        break;
    case EEXIST:
        strcpy(strerrbuf, "File exists");
        break;
    case EFAULT:
        strcpy(strerrbuf, "Bad address");
        break;
    case EFBIG:
        strcpy(strerrbuf, "File too large");
        break;
    case EHOSTUNREACH:
        strcpy(strerrbuf, "Host is unreachable");
        break;
    case EIDRM:
        strcpy(strerrbuf, "Identifier removed");
        break;
    case EILSEQ:
        strcpy(strerrbuf, "Illegal byte sequence");
        break;
    case EINPROGRESS:
        strcpy(strerrbuf, "Operation in progress");
        break;
    case EINTR:
        strcpy(strerrbuf, "Interrupted function");
        break;
    case EINVAL:
        strcpy(strerrbuf, "Invalid argument");
        break;
    case EIO:
        strcpy(strerrbuf, "I/O error");
        break;
    case EISCONN:
        strcpy(strerrbuf, "Socket is connected");
        break;
    case EISDIR:
        strcpy(strerrbuf, "Is a directory");
        break;
    case ELOOP:
        strcpy(strerrbuf, "Too many levels of symbolic links");
        break;
    case EMFILE:
        strcpy(strerrbuf, "File descriptor value too large");
        break;
    case EMLINK:
        strcpy(strerrbuf, "Too many links");
        break;
    case EMSGSIZE:
        strcpy(strerrbuf, "Message too large");
        break;
    case EMULTIHOP:
        strcpy(strerrbuf, "Reserved");
        break;
    case ENAMETOOLONG:
        strcpy(strerrbuf, "Filename too long");
        break;
    case ENETDOWN:
        strcpy(strerrbuf, "Network is down");
        break;
    case ENETRESET:
        strcpy(strerrbuf, "Connection aborted by network");
        break;
    case ENETUNREACH:
        strcpy(strerrbuf, "Network unreachable");
        break;
    case ENFILE:
        strcpy(strerrbuf, "Too many files open in system");
        break;
    case ENOBUFS:
        strcpy(strerrbuf, "No buffer space available");
        break;
    case ENODATA:
        strcpy(strerrbuf, "No message is available on the STREAM head read queue");
        break;
    case ENODEV:
        strcpy(strerrbuf, "No such device");
        break;
    case ENOENT:
        strcpy(strerrbuf, "No such file or directory");
        break;
    case ENOEXEC:
        strcpy(strerrbuf, "Executable file format error");
        break;
    case ENOLCK:
        strcpy(strerrbuf, "No locks available");
        break;
    case ENOLINK:
        strcpy(strerrbuf, "Reserved");
        break;
    case ENOMEM:
        strcpy(strerrbuf, "Not enough space");
        break;
    case ENOMSG:
        strcpy(strerrbuf, "No message of the desired type");
        break;
    case ENOPROTOOPT:
        strcpy(strerrbuf, "Protocol not available");
        break;
    case ENOSPC:
        strcpy(strerrbuf, "No space left on device");
        break;
    case ENOSR:
        strcpy(strerrbuf, "No STREAM resources");
        break;
    case ENOSTR:
        strcpy(strerrbuf, "Not a STREAM");
        break;
    case ENOSYS:
        strcpy(strerrbuf, "Functionality not supported");
        break;
    case ENOTCONN:
        strcpy(strerrbuf, "The socket is not connected");
        break;
    case ENOTDIR:
        strcpy(strerrbuf, "Not a directory or a symbolic link to a directory");
        break;
    case ENOTEMPTY:
        strcpy(strerrbuf, "Directory not empty");
        break;
    case ENOTRECOVERABLE:
        strcpy(strerrbuf, "State not recoverable");
        break;
    case ENOTSOCK:
        strcpy(strerrbuf, "Not a socket");
        break;
    case ENOTSUP:
        strcpy(strerrbuf, "Not supported");
        break;
    case ENOTTY:
        strcpy(strerrbuf, "Inappropriate I/O control operation");
        break;
    case ENXIO:
        strcpy(strerrbuf, "No such device or address");
        break;
    case EOPNOTSUPP:
        strcpy(strerrbuf, "Operation not supported on socket");
        break;
    case EOVERFLOW:
        strcpy(strerrbuf, "Value too large to be stored in data type");
        break;
    case EOWNERDEAD:
        strcpy(strerrbuf, "Previous owner died");
        break;
    case EPERM:
        strcpy(strerrbuf, "Operation not permitted");
        break;
    case EPIPE:
        strcpy(strerrbuf, "Broken pipe");
        break;
    case EPROTO:
        strcpy(strerrbuf, "Protocol error");
        break;
    case EPROTONOSUPPORT:
        strcpy(strerrbuf, "Protocol not supported");
        break;
    case EPROTOTYPE:
        strcpy(strerrbuf, "Protocol wrong type for socket");
        break;
    case ERANGE:
        strcpy(strerrbuf, "Result too large");
        break;
    case EROFS:
        strcpy(strerrbuf, "Read-only file system");
        break;
    case ESPIPE:
        strcpy(strerrbuf, "Invalid seek");
        break;
    case ESRCH:
        strcpy(strerrbuf, "No such process");
        break;
    case ESTALE:
        strcpy(strerrbuf, "Reserved");
        break;
    case ETIME:
        strcpy(strerrbuf, "Stream ioctl() timeout");
        break;
    case ETIMEDOUT:
        strcpy(strerrbuf, "Connection timed out");
        break;
    case ETXTBSY:
        strcpy(strerrbuf, "Text file busy");
        break;
    case EWOULDBLOCK:
        strcpy(strerrbuf, "Operation would block");
        break;
    case EXDEV:
        strcpy(strerrbuf, "Cross-device link");
        break;
    default:
        strcpy(strerrbuf, "Unknown");
    }
    return strerrbuf;
}
