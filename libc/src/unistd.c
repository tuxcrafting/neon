#include <errno.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <unistd.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Winvalid-noreturn"
noreturn void _exit(int status) {
    __asm__ volatile("int $0x80" :: "a"(SYSCALL_EXIT), "D"(status));
}
#pragma GCC diagnostic pop

int close(int fd) {
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "int $0x80;"
        "mov %%eax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_CLOSE), "m"(fd)
        : "%rax", "%rdi");
    return __errno_wrapper(r);
}

ssize_t write(int fd, const void *buf, size_t n) {
    ssize_t r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%rsi;"
        "mov %4, %%rdx;"
        "int $0x80;"
        "mov %%rax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_WRITE), "m"(fd), "m"(buf), "m"(n)
        : "%rax", "%rdi", "%rsi", "%rdx");
    return __errno_wrapper(r);
}

ssize_t pwrite(int fd, const void *buf, size_t n, off_t offset) {
    off_t soff = lseek(fd, 0, SEEK_CUR);
    if (lseek(fd, offset, SEEK_SET) == -1) {
        lseek(fd, soff, SEEK_SET);
        return -1;
    }
    ssize_t w = write(fd, buf, n);
    lseek(fd, soff, SEEK_SET);
    return w;
}

ssize_t read(int fd, void *buf, size_t n) {
    ssize_t r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%rsi;"
        "mov %4, %%rdx;"
        "int $0x80;"
        "mov %%rax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_READ), "m"(fd), "m"(buf), "m"(n)
        : "%rax", "%rdi", "%rsi", "%rdx");
    return __errno_wrapper(r);
}

ssize_t pread(int fd, void *buf, size_t n, off_t offset) {
    off_t soff = lseek(fd, 0, SEEK_CUR);
    if (lseek(fd, offset, SEEK_SET) == -1) {
        lseek(fd, soff, SEEK_SET);
        return -1;
    }
    ssize_t w = read(fd, buf, n);
    lseek(fd, soff, SEEK_SET);
    return w;
}

off_t lseek(int fd, off_t offset, int whence) {
    off_t r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%rsi;"
        "mov %4, %%edx;"
        "int $0x80;"
        "mov %%rax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_LSEEK), "m"(fd), "m"(offset), "m"(whence)
        : "%rax", "%rdi", "%rsi", "%rdx");
    return __errno_wrapper(r);
}

static int _fork() {
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "int $0x80;"
        "mov %%rax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_FORK)
        : "%rax");
    return __errno_wrapper(r);
}

static size_t fork_rbp;

__attribute__((naked)) int fork() {
    __asm__ volatile(
        "mov %%rbp, %0;"
        "call _fork;"
        "mov %0, %%rbp;"
        "ret"
        : "+m"(fork_rbp)
        : "i"(&_fork));
}

int fexecve(int fd, char *const argv[], char *const envp[]) {
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%rsi;"
        "mov %4, %%rdx;"
        "int $0x80;"
        "mov %%rax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_FEXECVE),
          "m"(fd),
          "m"(argv),
          "m"(envp)
        : "%rax", "%rdi", "%rsi", "%rdx");
    return __errno_wrapper(r);
}

int execve(const char *filename, char *const argv[], char *const envp[]) {
    int fd = open(filename, O_RDONLY | O_CLOEXEC);
    if (fd < 0) {
        return -1;
    }
    return fexecve(fd, argv, envp);
}
