#include <errno.h>
#include <sys/syscall.h>
#include <sys/wait.h>

int waitid(idtype_t idtype, id_t id, siginfo_t *infop, int options) {
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%esi;"
        "mov %4, %%rdx;"
        "mov %5, %%r8d;"
        "int $0x80;"
        "mov %%eax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_WAITID),
          "m"(idtype),
          "m"(id),
          "m"(infop),
          "m"(options)
        : "%rax", "%rdi", "%rsi", "%rdx", "%r8");
    return __errno_wrapper(r);
}
