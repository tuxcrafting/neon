#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/syscall.h>

int fstat(int fd, struct stat *buf) {
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%rsi;"
        "int $0x80;"
        "mov %%eax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_FSTAT),
          "m"(fd),
          "m"(buf)
        : "%rax", "%rdi", "%rsi");
    return __errno_wrapper(r);
}

int fstatat(int fd, const char *path, struct stat *buf, int flag) {
    int sfd = openat(fd, path, O_RDONLY | (flag ? O_NOFOLLOW_SUCCESS : 0));
    if (sfd < 0) {
        return sfd;
    }
    int r = fstat(sfd, buf);
    close(sfd);
    return r;
}

int stat(const char *path, struct stat *buf) {
    return fstatat(AT_FDCWD, path, buf, 0);
}

int lstat(const char *path, struct stat *buf) {
    return fstatat(AT_FDCWD, path, buf, 1);
}
