#include <errno.h>
#include <sys/mman.h>
#include <sys/syscall.h>

void *mmap(
        void *addr,
        size_t length,
        int prot,
        int flags,
        int fd,
        off_t offset) {
    if (flags & MAP_ANONYMOUS) {
        fd = MAPFD_ANON;
        flags &= ~MAP_ANONYMOUS;
    }
    size_t r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%rdi;"
        "mov %3, %%rsi;"
        "mov %4, %%edx;"
        "mov %5, %%r8d;"
        "mov %6, %%r9d;"
        "mov %7, %%r10;"
        "int $0x80;"
        "mov %%rax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_MMAP),
          "m"(addr),
          "m"(length),
          "m"(prot),
          "m"(flags),
          "m"(fd),
          "m"(offset)
        : "%rax", "%rdi", "%rsi", "%rdx", "%r8", "%r9", "%r10");
    return (void*)__errno_wrapper(r);
}

int munmap(void *addr, size_t length) {
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%rdi;"
        "mov %3, %%rsi;"
        "int $0x80;"
        "mov %%eax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_MUNMAP),
          "m"(addr),
          "m"(length)
        : "%rax", "%rdi", "%rsi");
    return __errno_wrapper(r);
}
