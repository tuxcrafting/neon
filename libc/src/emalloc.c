#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

// emalloc

// large block; a single mmap'd object for large allocations
#define LBLK_MAGIC 0x4110C001
// small block; forms a linked list of mmap'd objects for small allocations
#define SBLK_MAGIC 0x4110C005

// max size of a sblk allocation
#define SBLK_THRESHOLD (64 * 1024)
// minimum size of a sblk allocation
#define SBLK_ALLOC_BLOCK_SIZE 8

#define SBLK_BITMAP_ENTRIES (SBLK_THRESHOLD / SBLK_ALLOC_BLOCK_SIZE)
#define SBLK_BITMAP_SIZE (SBLK_BITMAP_ENTRIES / 4)

// size of a mmap page
#define PAGE_SIZE 4096
// size of a sblk
#define SBLK_SIZE (PAGE_SIZE + SBLK_THRESHOLD)

#define alloc_bit(b, i) ((b) & (1 << ((i) * 2)))
#define link_bit(b, i) ((b) & (1 << ((i) * 2 + 1)))
#define set_alloc_bit(b, i) (b |= (1 << ((i) * 2)))
#define set_link_bit(b, i) (b |= (1 << ((i) * 2 + 1)))
#define clear_alloc_bit(b, i) (b &= ~(1 << ((i) * 2)))
#define clear_link_bit(b, i) (b &= ~(1 << ((i) * 2 + 1)))

struct emalloc_blk {
    uint32_t magic;
};

struct emalloc_sblk {
    uint32_t magic;
    struct emalloc_sblk *next;
    size_t contiguous_free_space;
    uint8_t bitmap[SBLK_BITMAP_SIZE];
};

struct emalloc_lblk {
    uint32_t magic;
    size_t size;
};

static void *lblk_malloc(size_t size);
static void *sblk_malloc(size_t size);
static size_t get_size(void *ptr);

static struct emalloc_sblk *create_sblk();
static void *sblk_malloc_in(struct emalloc_sblk *blk, size_t size);
static void sblk_free_in(struct emalloc_sblk *blk, size_t index);
static void sblk_update_free_space(struct emalloc_sblk *blk);

struct emalloc_sblk *first_sblk;

void *malloc(size_t size) {
    if (size == 0) {
        return NULL;
    } else if (size > SBLK_THRESHOLD) {
        return lblk_malloc(size);
    } else {
        return sblk_malloc(size);
    }
}

void free(void *ptr) {
    if (ptr == NULL) {
        return;
    }
    struct emalloc_sblk **previous = NULL;
    struct emalloc_sblk **current = &first_sblk;
    while (*current != NULL) {
        void *base = (void*)*current + PAGE_SIZE;
        void *limit = base + SBLK_THRESHOLD;
        if (ptr >= base && ptr < limit) {
            sblk_free_in(*current, (ptr - base) / SBLK_ALLOC_BLOCK_SIZE);
            struct emalloc_sblk *current_tmp = *current;
            if (previous != NULL) {
                (*previous)->next = current_tmp->next;
            }
            if (current_tmp->contiguous_free_space == SBLK_THRESHOLD) {
                munmap(current_tmp, SBLK_SIZE);
            }
            return;
        }
        previous = current;
        current = &(*current)->next;
    }
    struct emalloc_lblk *blkptr = ptr - PAGE_SIZE;
    if (blkptr->magic == LBLK_MAGIC) {
        munmap(blkptr, blkptr->size + PAGE_SIZE);
    }
}

void *calloc(size_t nmemb, size_t size) {
    size *= nmemb;
    if (size == 0) {
        return NULL;
    } else if (size > SBLK_THRESHOLD) {
        return lblk_malloc(size);
    } else {
        void *m = sblk_malloc(size);
        memset(m, 0, size);
        return m;
    }
}

void *realloc(void *ptr, size_t size) {
    if (size == 0) {
        free(ptr);
        return NULL;
    }
    void *buf = malloc(size);
    if (buf == NULL) {
        return NULL;
    }
    memcpy(buf, ptr, get_size(ptr));
    free(ptr);
    return buf;
}

static void *lblk_malloc(size_t size) {
    void *ptr = mmap(
        NULL,
        size + PAGE_SIZE,
        PROT_READ | PROT_WRITE,
        0,
        MAPFD_ANON,
        0);
    if ((intptr_t)ptr < 0) {
        return NULL;
    }
    ((struct emalloc_lblk*)ptr)->magic = LBLK_MAGIC;
    ((struct emalloc_lblk*)ptr)->size = size;
    return ptr + PAGE_SIZE;
}

static void *sblk_malloc(size_t size) {
    struct emalloc_sblk **current = &first_sblk;
    while (*current != NULL) {
        if ((*current)->contiguous_free_space >= size) {
            return sblk_malloc_in(*current, size);
        }
        current = &(*current)->next;
    }
    if ((*current = create_sblk()) == NULL) {
        return NULL;
    }
    return sblk_malloc_in(*current, size);
}

static size_t get_size(void *ptr) {
    struct emalloc_sblk *current = first_sblk;
    while (current != NULL) {
        void *base = (void*)current + PAGE_SIZE;
        void *limit = base + SBLK_THRESHOLD;
        if (ptr >= base && ptr < limit) {
            size_t index = (ptr - base) / SBLK_ALLOC_BLOCK_SIZE;
            size_t length = 0;
            while (index < SBLK_BITMAP_ENTRIES) {
                uint8_t b = current->bitmap[index / 4];
                size_t i = index % 4;
                length++;
                if (!link_bit(b, i)) {
                    break;
                }
                index++;
            }
            return length;
        }
        current = current->next;
    }
    struct emalloc_lblk *blkptr = ptr - PAGE_SIZE;
    if (blkptr->magic == LBLK_MAGIC) {
        return blkptr->size;
    }
    return 0;
}

static struct emalloc_sblk *create_sblk() {
    struct emalloc_sblk *blk = mmap(
        NULL,
        SBLK_SIZE,
        PROT_READ | PROT_WRITE,
        0,
        MAPFD_ANON,
        0);
    if ((intptr_t)blk < 0) {
        return NULL;
    }
    blk->magic = SBLK_MAGIC;
    blk->contiguous_free_space = SBLK_THRESHOLD;
    return blk;
}

static void *sblk_malloc_in(struct emalloc_sblk *blk, size_t size) {
    if (size % SBLK_ALLOC_BLOCK_SIZE != 0) {
        size += SBLK_ALLOC_BLOCK_SIZE - size % SBLK_ALLOC_BLOCK_SIZE;
    }
    size /= SBLK_ALLOC_BLOCK_SIZE;
    size_t base = 0;
    size_t run = 0;
    for (size_t i = 0; i < SBLK_BITMAP_SIZE; i++) {
        uint8_t b = blk->bitmap[i];
        for (size_t j = 0; j < 4; j++) {
            if (alloc_bit(b, j)) {
                base = i * 4 + j + 1;
                run = 0;
            } else {
                run++;
                if (run == size) {
                    goto end_find;
                }
            }
        }
    }
    // this should normally *never* happen
    return NULL;
end_find:
    for (size_t i = 0; i < run; i++) {
        uint8_t *b = &blk->bitmap[i / 4];
        size_t j = i % 4;
        set_alloc_bit(*b, j);
        if (i != run - 1) {
            set_link_bit(*b, j);
        }
    }
    sblk_update_free_space(blk);
    return (void*)blk + PAGE_SIZE + base * SBLK_ALLOC_BLOCK_SIZE;
}

static void sblk_free_in(struct emalloc_sblk *blk, size_t index) {
    while (index < SBLK_BITMAP_ENTRIES) {
        uint8_t *b = &blk->bitmap[index / 4];
        size_t i = index % 4;
        clear_alloc_bit(*b, i);
        if (!link_bit(*b, i)) {
            break;
        }
        clear_link_bit(*b, i);
        index++;
    }
    sblk_update_free_space(blk);
}

static void sblk_update_free_space(struct emalloc_sblk *blk) {
    blk->contiguous_free_space = 0;
    size_t run = 0;
    for (size_t i = 0; i < SBLK_BITMAP_SIZE; i++) {
        uint8_t b = blk->bitmap[i];
        for (size_t j = 0; j < 4; j++) {
            if (alloc_bit(b, j)) {
                if (run > blk->contiguous_free_space) {
                    blk->contiguous_free_space = run;
                }
                run = 0;
            } else {
                run += SBLK_ALLOC_BLOCK_SIZE;
            }
        }
    }
}
