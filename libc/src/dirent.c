#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

DIR *opendir(const char *path) {
    int fd = open(path, O_RDONLY | O_DIRECTORY);
    if (fd < 0) {
        return NULL;
    }
    DIR *dir = fdopendir(fd);
    if (dir == NULL) {
        close(fd);
    }
    return dir;
}

DIR *fdopendir(int fd) {
    DIR *dir = malloc(sizeof(DIR));
    if (dir == NULL) {
        return NULL;
    }
    dir->fd = fd;
    return dir;
}

int dirfd(DIR *dir) {
    return dir->fd;
}

int closedir(DIR *dir) {
    return close(dir->fd);
}

struct dirent *readdir(DIR *dir) {
    static struct dirent dirent;
    int n = read(dir->fd, &dirent, sizeof(struct dirent));
    if (n > 0) {
        return &dirent;
    }
    return NULL;
}
