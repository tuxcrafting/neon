#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE __stdin = { STDIN_FILENO, 0 };
FILE __stdout = { STDOUT_FILENO, 0 };
FILE __stderr = { STDERR_FILENO, 0 };

FILE *stdin = &__stdin;
FILE *stdout = &__stdout;
FILE *stderr = &__stderr;

static int parse_mode(const char *mode) {
    int m = 0;
    while (*mode) {
        switch (*mode++) {
        case 'r':
            m |= O_RDONLY;
            break;
        case 'w':
            m |= O_WRONLY | O_CREAT | O_TRUNC;
            break;
        case 'a':
            m |= O_APPEND | O_CREAT | O_TRUNC;
            break;
        case '+':
            m |= O_RDWR;
            break;
        }
    }
    return m;
}

FILE *fopen(const char *path, const char *mode) {
    int fd = open(path, parse_mode(mode));
    if (fd < 0) {
        return NULL;
    }
    FILE *stream = fdopen(fd, mode);
    if (stream == NULL) {
        close(fd);
    }
    return stream;
}

FILE *fdopen(int fd, const char *mode) {
    FILE *stream = malloc(sizeof(FILE));
    if (stream == NULL) {
        return NULL;
    }
    stream->fd = fd;
    stream->state = __FILE_state_OK;
    return stream;
}

int fclose(FILE *stream) {
    return close(fileno(stream));
}

int fputc(int c, FILE *stream) {
    return write(fileno(stream), &c, 1);
}

int putc(int c, FILE *stream) {
    return fputc(c, stream);
}

int putchar(int c) {
    return fputc(c, stdout);
}

int fputs(const char *s, FILE *stream) {
    return write(fileno(stream), s, strlen(s));
}

int puts(const char *s) {
    if (fputs(s, stdout) < 0) {
        return -1;
    }
    return putchar('\n');
}

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t items = 0;
    for (size_t i = 0; i < nmemb * size; i += size) {
        ssize_t x = read(fileno(stream), ptr + i, size);
        if (x == 0) {
            stream->state = __FILE_state_EOF;
            break;
        } else if (x < 0) {
            stream->state = __FILE_state_ERROR;
            break;
        }
        items++;
    }
    return items;
}

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t items = 0;
    for (size_t i = 0; i < nmemb * size; i += size) {
        if (write(fileno(stream), ptr + i, size) < 0) {
            stream->state = __FILE_state_ERROR;
            break;
        }
        items++;
    }
    return items;
}

int fseeko(FILE *stream, off_t offset, int whence) {
    return lseek(fileno(stream), offset, whence);
}

off_t ftello(FILE *stream) {
    return fseek(stream, 0, SEEK_CUR);
}

int fseek(FILE *stream, long offset, int whence) {
    return fseeko(stream, offset, whence);
}

long ftell(FILE *stream) {
    return ftello(stream);
}

void rewind(FILE *stream) {
    fseeko(stream, 0, SEEK_SET);
}

int fgetpos(FILE *stream, fpos_t *pos) {
    return (*pos = ftello(stream));
}

int fsetpos(FILE *stream, const fpos_t *pos) {
    return fseeko(stream, *pos, SEEK_SET);
}
