#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/syscall.h>

int openat(int fd, const char *path, int oflag) {
    size_t plen = strlen(path);
    int r;
    __asm__ volatile(
        "mov %1, %%rax;"
        "mov %2, %%edi;"
        "mov %3, %%rsi;"
        "mov %4, %%rdx;"
        "mov %5, %%r8;"
        "mov %6, %%r9d;"
        "int $0x80;"
        "mov %%eax, %0;"
        : "=m"(r)
        : "i"(SYSCALL_OPENAT),
          "m"(fd),
          "m"(path),
          "m"(plen),
          "m"(oflag),
          "i"(0)
        : "%rax", "%rdi", "%rsi", "%rdx", "%r8", "%r9");
    return __errno_wrapper(r);
}

int open(const char *path, int oflag) {
    return openat(AT_FDCWD, path, oflag);
}
