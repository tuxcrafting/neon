#include <stdlib.h>

noreturn void exit(int status) {
    _exit(status);
}
