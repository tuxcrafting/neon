all: ;

DC   := ldc2
NASM := nasm
LD   := ld

DCFLAGS ?=
DCFLAGS += -disable-red-zone -defaultlib= -m64 -betterC -mattr=-sse -g

NASMFLAGS ?=
NASMFLAGS += -f elf64

%.o: %.d
	$(DC) $(DCFLAGS) -c -of=$@ $<

%.o: %.asm
	$(NASM) $(NASMFLAGS) -o $@ $<

KERNEL_SRC_D   := $(shell find kernel -name '*.d')
KERNEL_SRC_ASM := $(shell find kernel -name '*.asm')
KERNEL_OBJ     := \
	$(KERNEL_SRC_D:%.d=%.o) \
	$(KERNEL_SRC_ASM:%.asm=%.o)
USERLAND_BIN   := $(wildcard userland/bin/*)
INITRD_BIN     := $(USERLAND_BIN:userland/bin/%=initrd/bin/%)

MKTDIR = mkdir -p $@

iso/boot:
	$(MKTDIR)
iso/boot/grub:
	$(MKTDIR)
initrd:
	$(MKTDIR)
initrd/bin:
	$(MKTDIR)
initrd/dev:
	$(MKTDIR)

initrd/bin/%: userland/bin/% | initrd/bin
	cp $< $@

iso/boot/neon-initrd: $(INITRD_BIN) | initrd/dev
	cd initrd && find . -depth -print | cpio -o -H odc > ../$@

iso/boot/neon: kernel/linker.ld $(KERNEL_OBJ) | iso/boot
	$(LD) -nostdlib -nodefaultlibs -o $@ -T $< $(filter %.o,$(KERNEL_OBJ))

neon.iso: conf/grub.cfg iso/boot/neon iso/boot/neon-initrd | iso/boot/grub
	cp $< iso/boot/grub/
	grub-mkrescue -o $@ iso

all: neon.iso

run: neon.iso
	qemu-system-x86_64 -monitor stdio -no-reboot -m 256M -cdrom $<

debug: neon.iso
	qemu-system-x86_64 -monitor stdio -no-reboot -m 256M -d int,in_asm -cdrom $<

clean:
	rm -f $(KERNEL_OBJ) neon.iso
	rm -rf iso initrd
