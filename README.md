# Neon

Neon is a POSIX-compatible hobby OS. Or, more exactly, will be.
It doesn't have a lot of features currently.

## How to build

`./build.sh`

## How to run

`make run` (or `make debug`) to launch it in qemu.
