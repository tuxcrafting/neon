#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

extern char **environ;

char delim[] = " \n\t";

int main(int argc, char *argv[]) {
    puts("cx (command executive) 0.1 for Neon");
    for (;;) {
        fputs("% ", stdout);
        char buffer[512];
        buffer[read(0, buffer, 512)] = 0;
        char *name = strtok(buffer, delim);
        char *args[32];
        args[0] = name;
        size_t argi = 1;
        char *ptr;
        while ((ptr = strtok(NULL, delim))) {
            args[argi++] = ptr;
        }
        args[argi] = NULL;
        int pid = fork();
        if (pid < 0) {
            puts("Error: can't fork");
        } else if (pid == 0) {
            execve(name, args, environ);
            puts("Error: can't execute");
            return 1;
        } else {
            waitid(P_PID, pid, NULL, WEXITED);
        }
    }
}
