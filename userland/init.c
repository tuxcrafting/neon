#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

char *args[] = {"/bin/cx", NULL};
char *env[] = {NULL};

int main(int argc, char *argv[]) {
    open("/dev/001", O_RDONLY); // stdin
    open("/dev/000", O_WRONLY); // stdout
    open("/dev/000", O_WRONLY); // stderr
    int pid = fork();
    if (pid < 0) {
        puts("Error: couldn't fork");
    } else if (pid == 0) {
        execve(args[0], args, env);
        puts("Error: couldn't pass execution to cx");
        return 1;
    } else {
        for (;;);
    }
    return 1;
}
