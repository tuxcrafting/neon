#include <errno.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        puts("Usage: ls <dir>");
        return 1;
    }
    errno = 0;
    DIR *dir = opendir(argv[1]);
    if (dir == NULL) {
        if (errno != 0) {
            puts(strerror(errno));
        }
        return 1;
    }
    for (;;) {
        errno = 0;
        struct dirent *entry = readdir(dir);
        if (entry == NULL) {
            if (errno != 0) {
                puts(strerror(errno));
            }
            break;
        }
        puts(entry->d_name);
    }
    closedir(dir);
    return 0;
}
