#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc < 1) {
        return 0;
    }
    int flag_n = 0;
    int i = 1;
    if (strcmp(argv[i], "-n") == 0) {
        flag_n = 1;
        i++;
    }
    for (; i < argc; i++) {
        fputs(argv[i], stdout);
        if (i != argc - 1) {
            putchar(' ');
        }
    }
    if (!flag_n) {
        putchar('\n');
    }
    return 0;
}
